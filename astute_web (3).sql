-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 28, 2023 at 05:59 PM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `astute_web`
--

-- --------------------------------------------------------

--
-- Table structure for table `complaints`
--

CREATE TABLE `complaints` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cpf` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `complaint_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `complaint_category` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telephone_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `complaint_discription` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_of_availibity` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time_from` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time_to` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `other_remarks` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `devices`
--

CREATE TABLE `devices` (
  `id` int(10) UNSIGNED NOT NULL,
  `device_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `IMEI_no` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `device_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `json_file` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gmail` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sensor` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_update` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `devices`
--

INSERT INTO `devices` (`id`, `device_id`, `IMEI_no`, `device_name`, `user_id`, `url`, `json_file`, `gmail`, `password`, `sensor`, `last_update`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 'AST0001', '123456789', 'AST_0001', 4, 'https://esp32-ee967.firebaseio.com', 'esp32-ee967-firebase-adminsdk-ydox6-59c5c497dc.json', 'sixthsensegecr@gmail.com', 'weare@2022', '[\"1\",\"2\"]', NULL, 1, '2022-07-17 07:50:01', '2022-09-10 13:28:59'),
(2, 'AST002', '123', 'AST_0002', 4, 'https://ast0002-default-rtdb.firebaseio.com', 'ast0002-firebase-adminsdk-z7uie-7500fed480.json', 'ast002@gmail.com', '123', '[\"1\",\"2\"]', NULL, 1, '2022-09-08 12:18:33', '2023-01-07 23:35:13');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2022_04_20_055640_create_complaints_table', 1),
(6, '2022_05_15_171251_laratrust_setup_tables', 1),
(9, '2022_07_09_122528_create_tests_table', 4),
(10, '2022_05_22_104531_create_devices_table', 5),
(11, '2022_05_24_171917_create_sensors_table', 6),
(12, '2022_10_16_045717_create_sensor_data_table', 7);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'user-permission', 'crud Users', 'crud Users', '2022-05-17 11:36:16', '2022-05-17 11:36:16'),
(2, 'role-permission', 'crud Role', 'crud Role', '2022-05-17 11:36:16', '2022-05-17 11:36:16'),
(3, 'permission', 'crud Permission', 'crud Permission', '2022-05-17 11:36:16', '2022-05-17 11:36:16'),
(4, 'simple-userpermisssion', 'simple-userpermisssion', 'simple user permission', '2022-06-18 23:09:59', '2022-06-18 23:09:59');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 2);

-- --------------------------------------------------------

--
-- Table structure for table `permission_user`
--

CREATE TABLE `permission_user` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `user_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_user`
--

INSERT INTO `permission_user` (`permission_id`, `user_id`, `user_type`) VALUES
(1, 1, 'App\\Models\\User'),
(2, 1, 'App\\Models\\User'),
(3, 1, 'App\\Models\\User'),
(4, 4, 'App\\Models\\User'),
(4, 5, 'App\\Models\\User');

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'User Administrator', 'User is allowed to manage and edit other users', '2022-05-17 11:36:16', '2022-05-17 11:36:16'),
(2, 'user', 'user', 'normal user', '2022-06-18 23:09:24', '2022-06-18 23:09:24');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `user_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`role_id`, `user_id`, `user_type`) VALUES
(1, 1, 'App\\Models\\User'),
(2, 4, 'App\\Models\\User'),
(2, 5, 'App\\Models\\User');

-- --------------------------------------------------------

--
-- Table structure for table `sensors`
--

CREATE TABLE `sensors` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `max` int(11) NOT NULL,
  `min` int(11) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sensors`
--

INSERT INTO `sensors` (`id`, `name`, `max`, `min`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 'V', 5, 0, 1, '2022-09-04 11:14:58', '2022-09-04 11:14:58'),
(2, 'I', 50, 0, 1, '2022-09-08 12:19:07', '2022-09-08 12:19:07');

-- --------------------------------------------------------

--
-- Table structure for table `sensor_data`
--

CREATE TABLE `sensor_data` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `device_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sensor_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sensor_data`
--

INSERT INTO `sensor_data` (`id`, `user_id`, `device_id`, `sensor_id`, `time`, `value`, `created_at`, `updated_at`) VALUES
(1, '4', '1', '1', '1653132402', '0.00', '2022-10-17 11:53:43', '2022-10-17 11:53:43'),
(2, '4', '1', '1', '1653132402', '1.00', '2022-10-17 11:53:43', '2022-10-17 11:53:43'),
(3, '4', '1', '1', '1653132402', '2.00', '2022-10-17 11:53:43', '2022-10-17 11:53:43'),
(4, '4', '1', '1', '1653132402', '3.00', '2022-10-17 11:53:43', '2022-10-17 11:53:43'),
(5, '4', '1', '1', '1653132402', '4.00', '2022-10-17 11:53:43', '2022-10-17 11:53:43'),
(6, '4', '1', '1', '1653132402', '5.00', '2022-10-17 11:53:43', '2022-10-17 11:53:43'),
(7, '4', '1', '1', '1653132402', '6.00', '2022-10-17 11:53:43', '2022-10-17 11:53:43'),
(8, '4', '1', '1', '1653132402', '7.00', '2022-10-17 11:53:43', '2022-10-17 11:53:43'),
(9, '4', '1', '1', '1653132402', '8.00', '2022-10-17 11:53:43', '2022-10-17 11:53:43'),
(10, '4', '1', '1', '1653132402', '9.00', '2022-10-17 11:53:43', '2022-10-17 11:53:43'),
(11, '4', '1', '1', '1653132402', '10.00', '2022-10-17 11:53:43', '2022-10-17 11:53:43'),
(12, '4', '1', '1', '1653132402', '11.00', '2022-10-17 11:53:43', '2022-10-17 11:53:43'),
(13, '4', '1', '1', '1653132402', '12.00', '2022-10-17 11:53:43', '2022-10-17 11:53:43'),
(14, '4', '1', '2', '1653132402', '0.00', '2022-10-17 11:53:43', '2022-10-17 11:53:43'),
(15, '4', '1', '2', '1653132402', '1.00', '2022-10-17 11:53:43', '2022-10-17 11:53:43'),
(16, '4', '1', '2', '1653132402', '2.00', '2022-10-17 11:53:43', '2022-10-17 11:53:43'),
(17, '4', '1', '2', '1653132402', '3.00', '2022-10-17 11:53:44', '2022-10-17 11:53:44'),
(18, '4', '1', '2', '1653132402', '4.00', '2022-10-17 11:53:44', '2022-10-17 11:53:44'),
(19, '4', '1', '2', '1653132402', '5.00', '2022-10-17 11:53:44', '2022-10-17 11:53:44'),
(20, '4', '1', '2', '1653132402', '6.00', '2022-10-17 11:53:44', '2022-10-17 11:53:44'),
(21, '4', '1', '2', '1653132402', '7.00', '2022-10-17 11:53:44', '2022-10-17 11:53:44'),
(22, '4', '1', '2', '1653132402', '8.00', '2022-10-17 11:53:44', '2022-10-17 11:53:44'),
(23, '4', '1', '2', '1653132402', '9.00', '2022-10-17 11:53:44', '2022-10-17 11:53:44'),
(24, '4', '1', '2', '1653132402', '10.00', '2022-10-17 11:53:44', '2022-10-17 11:53:44'),
(25, '4', '1', '2', '1653132402', '11.00', '2022-10-17 11:53:44', '2022-10-17 11:53:44'),
(26, '4', '1', '2', '1653132402', '12.00', '2022-10-17 11:53:44', '2022-10-17 11:53:44'),
(27, '4', '2', '1', '1653132402', '20.00', '2022-10-29 12:39:16', '2022-10-29 12:39:16'),
(28, '4', '2', '1', '1653132402', '21.00', '2022-10-29 12:39:16', '2022-10-29 12:39:16'),
(29, '4', '2', '1', '1653132402', '22.00', '2022-10-29 12:39:16', '2022-10-29 12:39:16'),
(30, '4', '2', '1', '1653132402', '23.00', '2022-10-29 12:39:16', '2022-10-29 12:39:16'),
(31, '4', '2', '1', '1653132402', '24.00', '2022-10-29 12:39:16', '2022-10-29 12:39:16'),
(32, '4', '2', '1', '1653132402', '25.00', '2022-10-29 12:39:16', '2022-10-29 12:39:16'),
(33, '4', '2', '1', '1653132402', '26.00', '2022-10-29 12:39:16', '2022-10-29 12:39:16'),
(34, '4', '2', '1', '1653132402', '27.00', '2022-10-29 12:39:16', '2022-10-29 12:39:16'),
(35, '4', '2', '1', '1653132402', '28.00', '2022-10-29 12:39:16', '2022-10-29 12:39:16'),
(36, '4', '2', '1', '1653132402', '29.00', '2022-10-29 12:39:16', '2022-10-29 12:39:16'),
(37, '4', '2', '1', '1653132402', '30.00', '2022-10-29 12:39:16', '2022-10-29 12:39:16'),
(38, '4', '2', '1', '1653132402', '31.00', '2022-10-29 12:39:16', '2022-10-29 12:39:16'),
(39, '4', '2', '1', '1653132402', '32.00', '2022-10-29 12:39:16', '2022-10-29 12:39:16'),
(40, '4', '2', '2', '1653132402', '40.00', '2022-10-29 12:39:16', '2022-10-29 12:39:16'),
(41, '4', '2', '2', '1653132402', '41.00', '2022-10-29 12:39:16', '2022-10-29 12:39:16'),
(42, '4', '2', '2', '1653132402', '42.00', '2022-10-29 12:39:16', '2022-10-29 12:39:16'),
(43, '4', '2', '2', '1653132402', '43.00', '2022-10-29 12:39:16', '2022-10-29 12:39:16'),
(44, '4', '2', '2', '1653132402', '44.00', '2022-10-29 12:39:16', '2022-10-29 12:39:16'),
(45, '4', '2', '2', '1653132402', '45.00', '2022-10-29 12:39:16', '2022-10-29 12:39:16'),
(46, '4', '2', '2', '1653132402', '46.00', '2022-10-29 12:39:16', '2022-10-29 12:39:16'),
(47, '4', '2', '2', '1653132402', '47.00', '2022-10-29 12:39:16', '2022-10-29 12:39:16'),
(48, '4', '2', '2', '1653132402', '48.00', '2022-10-29 12:39:16', '2022-10-29 12:39:16'),
(49, '4', '2', '2', '1653132402', '49.00', '2022-10-29 12:39:16', '2022-10-29 12:39:16'),
(50, '4', '2', '2', '1653132402', '50.00', '2022-10-29 12:39:17', '2022-10-29 12:39:17'),
(51, '4', '2', '2', '1653132402', '51.00', '2022-10-29 12:39:17', '2022-10-29 12:39:17'),
(52, '4', '2', '2', '1653132402', '52.00', '2022-10-29 12:39:17', '2022-10-29 12:39:17'),
(53, '4', '1', '2', '1653132403', '13', '2022-11-27 12:50:51', '2022-11-27 12:50:51'),
(54, '4', '1', '2', '1653132403', '14', '2022-11-27 12:51:26', '2022-11-27 12:51:26'),
(55, '4', '1', '2', '1653132405', '14', '2022-11-27 12:51:46', '2022-11-27 12:51:46'),
(56, '4', '2', '2', '1653132404', '60.00', '2022-11-29 12:30:25', '2022-11-29 12:30:25'),
(57, '4', '2', '2', '1653132404', '65.00', '2022-11-29 12:30:46', '2022-11-29 12:30:46'),
(58, '4', '1', '2', '1653132404', '49.00', '2022-11-29 12:31:41', '2022-11-29 12:31:41'),
(59, '4', '1', '2', '1653132410', '20', '2022-11-30 12:35:24', '2022-11-30 12:35:24'),
(60, '4', '1', '2', '1653132410', '5', '2022-11-30 12:35:47', '2022-11-30 12:35:47'),
(61, '4', '1', '2', '1653132410', '38', '2022-11-30 12:36:32', '2022-11-30 12:36:32'),
(62, '4', '1', '2', '1653132411', '38', '2022-11-30 12:43:47', '2022-11-30 12:43:47');

-- --------------------------------------------------------

--
-- Table structure for table `tests`
--

CREATE TABLE `tests` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tests`
--

INSERT INTO `tests` (`id`, `name`, `type`, `created_at`, `updated_at`) VALUES
(1, 'new', 'civil', '2022-07-09 12:38:36', '2022-07-09 12:38:36'),
(2, 'test', 'civil', '2022-07-09 12:38:36', '2022-07-09 12:38:36'),
(3, 'new name', 'infocom', '2022-07-09 12:39:01', '2022-07-09 12:39:01'),
(4, 'test name', 'infocom', '2022-07-09 12:39:18', '2022-07-09 12:39:18');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT 1,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `contact_no`, `is_active`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@gmail.com', '$2y$10$W.WGBBaUWTNoIJBP5gPsE.aOmjrrBr2MsZH.U9mTIBomlg49br4Li', NULL, 1, NULL, '2022-05-17 11:36:16', '2022-05-17 11:36:16'),
(4, 'test', 'test@gmail.com', '$2y$10$3c4VlLolq/3.5UHbhxHWTujbjygifbTJxvUYLLAAElbFcLmcu3d2.', NULL, 1, NULL, '2022-05-20 12:15:15', '2023-01-01 08:43:08'),
(5, 'ast', 'ast@gmail.com', '$2y$10$xKzLpwqlt8r65vPbzPhzROl3S71F0BisYCAqWoDbtCrreuMTL.g1q', '1234567890', 1, NULL, '2022-06-18 23:24:03', '2022-08-17 22:54:18');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `complaints`
--
ALTER TABLE `complaints`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `devices`
--
ALTER TABLE `devices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indexes for table `permission_user`
--
ALTER TABLE `permission_user`
  ADD PRIMARY KEY (`user_id`,`permission_id`,`user_type`),
  ADD KEY `permission_user_permission_id_foreign` (`permission_id`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`user_id`,`role_id`,`user_type`),
  ADD KEY `role_user_role_id_foreign` (`role_id`);

--
-- Indexes for table `sensors`
--
ALTER TABLE `sensors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sensor_data`
--
ALTER TABLE `sensor_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tests`
--
ALTER TABLE `tests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `complaints`
--
ALTER TABLE `complaints`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `devices`
--
ALTER TABLE `devices`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `sensors`
--
ALTER TABLE `sensors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `sensor_data`
--
ALTER TABLE `sensor_data`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT for table `tests`
--
ALTER TABLE `tests`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `permission_user`
--
ALTER TABLE `permission_user`
  ADD CONSTRAINT `permission_user_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
