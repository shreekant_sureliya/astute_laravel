<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\AuthController;
use App\Http\Controllers\LogoutController;
use App\Http\Controllers\ManageUserController;
use App\Http\Controllers\ManageDeviceController;
use App\Http\Controllers\ManageSensorController;
use App\Http\Controllers\SaveDataController;
use App\Http\Controllers\UserDashboardController;
use App\Http\Controllers\TableDataController;
use App\Http\Controllers\TestController;
use App\Http\Controllers\GraphController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('auth_device/{imei}',[SaveDataController::class,'auth_device'])->name('auth_device');

Route::get('/', [AuthController::class, 'login'])->name('login');
Route::get('/login', [AuthController::class, 'login'])->name('login');
Route::post('/login', [AuthController::class, 'do_login'])->name('do_login');
Route::post('/logout', [LogoutController::class, 'logout'])->name('logout');

Route::get('/home', [HomeController::class, 'index'])->name('home');

Route::group(['middleware' => ['role:admin']], function () {

    //--------------------------------- User Manage---------------------------------
    Route::get('/user-manage', [ManageUserController::class, 'index'])->name('user-manage');
    Route::get('/create-user', [ManageUserController::class, 'create'])->name('user.create');
    Route::post('/save-user', [ManageUserController::class, 'save'])->name('user.save');
    Route::get('/edit-user/{id}', [ManageUserController::class, 'edit'])->name('user.edit');
    Route::post('/update-user/{id}', [ManageUserController::class, 'update'])->name('user.update');
    Route::post('active-user', [ManageUserController::class, 'active_user'])->name('active-user');

    //--------------------------------- Device Manage---------------------------------
    Route::get('/device-manage', [ManageDeviceController::class, 'index'])->name('device-manage');
    Route::get('/create-device', [ManageDeviceController::class, 'create'])->name('device.create');
    Route::post('/save-device', [ManageDeviceController::class, 'save'])->name('device.save');
    Route::get('/edit-device/{id}', [ManageDeviceController::class, 'edit'])->name('device.edit');
    Route::post('/update-device/{id}', [ManageDeviceController::class, 'update'])->name('device.update');

    //--------------------------------- Sensor Manage---------------------------------
    Route::get('/sensor-manage', [ManageSensorController::class, 'index'])->name('sensor-manage');
    Route::get('/create-sensor', [ManageSensorController::class, 'create'])->name('sensor.create');
    Route::post('/save-sensor', [ManageSensorController::class, 'save'])->name('sensor.save');
    Route::get('/edit-sensor/{id}', [ManageSensorController::class, 'edit'])->name('sensor.edit');
    Route::post('/update-sensor/{id}', [ManageSensorController::class, 'update'])->name('sensor.update');
    Route::post('active-sensor', [ManageSensorController::class, 'active_sensor'])->name('active-sensor');


});

Route::group(['middleware' => ['role:user']], function () {

    //--------------------------------- User Dashboard Manage---------------------------------
    Route::get('user-dashboard', [UserDashboardController::class, 'index'])->name('user-dashboard');
    Route::post('get_graph', [UserDashboardController::class, 'get_graph'])->name('get_graph');

    //---------------------------------------- graph------------------------------------------
    Route::get('graph',[GraphController::class,'index'])->name('graph');
    Route::post('get_graph_data', [GraphController::class, 'get_graph_data'])->name('get_graph_data');

    //---------------------------- Data manage table---------------------------
    Route::get('table-data', [TableDataController::class, 'index'])->name('table-data');
    Route::post('get-data', [TableDataController::class, 'getdata'])->name('get-data');

});

// Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
