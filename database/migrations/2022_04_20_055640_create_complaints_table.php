<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('complaints', function (Blueprint $table) {
            $table->id();
            $table->string('cpf');
            $table->string('complaint_type');
            $table->string('complaint_category');
            $table->string('telephone_no')->nullable();
            $table->string('complaint_discription');
            $table->string('location')->nullable();
            $table->string('address')->nullable();
            $table->string('date_of_availibity');
            $table->string('time_from');
            $table->string('time_to');
            $table->string('other_remarks')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('complaints');
    }
};
