<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Complaint;
use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use App\Models\Device;
use Auth;
use Illuminate\Support\Facades\Hash;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;

class SaveDataController extends Controller
{
    
    public function index(Request $request)
    {
      
        $data=$request->data;
        $v=$request->V;
        $i=$request->I;

        $sep_data=explode(",",$data);
        $device_id='AST_'.$sep_data[0];

        $device = Device::where('device_id',$device_id)->first();
        

        //initialize firebase
        $serviceAccount = ServiceAccount::fromJsonFile($device->jsonpath);
        $firebase = (new Factory)
        ->withServiceAccount($serviceAccount)
        ->withDatabaseUri($device->url)
        ->create();
        $database = $firebase->getDatabase();
        //-------------------------------------------------------------------------------------------------------

        $date_time=$sep_data[1];
        if($date_time == '0'){
            $device = Device::where('device_id',$device_id)->first();
            $date_time=$device->last_update;
        }
            //$all_data=array_slice($sep_data, 2);

            //-----put data into firebase-----------
            $updates = [];
            $v_data=explode(',' ,$v);
            $i_data=explode(',' ,$i);
            
            for ($x = 0; $x < count($v_data); $x++) {
                $temp=[];
                $temp['V']=$v_data[$x];
                $temp['I']=$i_data[$x];
                $updates[$date_time++]=$temp;
            }
            
            $database->getReference('data')->update($updates);
            //----------------------------------------------

            $device = Device::where('device_id',$device_id)->first();
            $device->last_update = $date_time;
            $device->save();

            echo "<br>finished";

    }

    public function auth_device($imei)
    {
        $imei=$imei;
        if($imei != null){
            $device = Device::where('IMEI_no',$imei)->first();
            $url = $device->url;
            $time = time();
            return $url.','.$time;
        }
        return '0';
    }
}
