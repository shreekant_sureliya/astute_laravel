<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Device;
use App\Models\Sensor;
use Auth;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use File;

class UserDashboardController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      $this->middleware('auth');        
  }

  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Contracts\Support\Renderable
   */
  public function index()
  {   
    $user_id = Auth::user()->id;
    $device = Device::where('user_id',$user_id)->get();
    $device_count = $device->count();
    //return view('user dashboard.dashboard',compact('device','device_count'));
    return view('user dashboard.dashboard_6X6',compact('device','device_count'));
    
  }

  public function get_graph(Request $request){
      
    $user_id = Auth::user()->id;
    $device = Device::where('user_id',$user_id)->get();
    $no = $request->no;
    $i=1;
    $j=1;
    foreach($device as $d){
      //$serviceAccount = ServiceAccount::fromJsonFile($d->jsonpath);
      $firebase = (new Factory)
      ->withServiceAccount($d->jsonpath)
      ->withDatabaseUri($d->url)
      ->createDatabase();
      $database = $firebase;
      
      $data=$database->getReference('/')
        ->limitToLast($no)
        ->orderByValue()
        ->getSnapshot();
  
      $data=$data->getValue();


      $xvalues=[];
      $yvalues=[];

      //date_default_timezone_set("Asia/Kolkata");

      $sensors = Sensor::whereIn('id',json_decode($d->sensor))->get();
      foreach($data as $key=>$val){
        $xvalues[] = date('h:i:s', $key);
        foreach($sensors as $sensor){
          $yvalues[$sensor->name][] = $val[$sensor->name];
        }
      }

      $out['type'] = 'success';
      $out['xvalue'][$i] = $xvalues;
      foreach($sensors as $sensor){
          $out['yvalues_'.$sensor->name][$i] = $yvalues[$sensor->name];
      }
      $i++;
    }
    return json_encode($out);
  }
}
