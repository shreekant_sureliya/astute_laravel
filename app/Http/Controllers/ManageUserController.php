<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Complaint;
use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Auth;
use Illuminate\Support\Facades\Hash;
use File;

class ManageUserController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      $this->middleware('auth');
  }

  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Contracts\Support\Renderable
   */
  public function index()
  {
    
    /* $file = File::get(public_path('firebase_json/ast0002-firebase-adminsdk-z7uie-7500fed480.json'));
    dd($file); */

    $data = User::whereHas(
      'roles', function($q){
          $q->where('name', 'user');
      }
    )->paginate(10);
      return view('user.user_list',compact('data'));
    }

  public function create(){
    return view('user.user_create');
  }

  public function edit($id){
    if(isset($id)){
      $user = User::find($id);
      return view('user.user_edit',compact('user'));
    }      
  }

  public function update($id,Request $request){      
    $request->validate([
      'name' => 'required|unique:users,name,'.$id,
      'password' => 'required',
      'email' => 'required|unique:users,email,'.$id,
    ],[
      'name.required' => 'This Name Field is required',
      'password.required' => 'This Password Field is required',        
      'email.required' => 'This Email Field is required',
    ]);

    $user = User::find($id);
    $user->name = $request->name;
    $user->email = $request->email;
    $user->contact_no = $request->contact_no;
    if($user->password == $request->password ){
      
    }
    else{
      $user->password = bcrypt($request->password);
    }
    $user->save();
    $request->session()->flash('message', 'User Update successfully');
    return redirect()->route('user-manage');
  }

  public function save(Request $request){      
    $request->validate([
      'name' => 'required|unique:users',
      'password' => 'required',
      'email' => 'required|unique:users',
    ],[
      'name.required' => 'This Name Field is required',
      'password.required' => 'This Password Field is required',        
      'email.required' => 'This Email Field is required',
    ]);

    $user = new User;
    $user->name = $request->name;
    $user->email = $request->email;
    $user->contact_no = $request->contact_no;
    $user->password = bcrypt($request->password);
    $user->save();

    $user->syncPermissions(['simple-userpermisssion']);
    $user->syncRoles(['user']);

    $request->session()->flash('message', 'User Add successfully');
    return redirect()->route('user-manage');
  }

  public function active_user(Request $request){

    $id = $request->id;
    $is_active = $request->is_active;

    if(isset($id) && isset($is_active)){
      $user = User::find($id);
      $user->is_active = $is_active == 'true' ? 1 : 0;
      $user->save();
  
      $out['type'] = 'success';
    }else{
      $out['type'] = 'error';
    }
    echo json_encode($out);
    die;

  }
}
