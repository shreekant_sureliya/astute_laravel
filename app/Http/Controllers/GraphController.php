<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Device;
use App\Models\Sensor;
use Auth;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Carbon\Carbon;
use DateTime;


class GraphController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user_id = Auth::user()->id;
        $devices = Device::where('user_id',$user_id)->get();
        $sensors = Sensor::get();
        return view('graph.graph',compact('devices','sensors'));
        //return view('graph.button_graph');
    }


    public function get_graph_data(Request $request){

      $user_id = Auth::user()->id;
      $device_id = $request->device_id;
      $sensor_id = $request->sensor_id;
      $date = $request->date;
      $device = Device::where([['user_id',$user_id],['id',$device_id]])->first();

      if($device != null && $device_id != null && $sensor_id != null && $date != null){

        $date = explode('-',$date);
        $f = str_replace('/', '-', $date[0]);
        $from_date = date('Y-m-d', strtotime($f));
        $dtf   = new DateTime($from_date);

        $t = str_replace('/', '-', $date[1]);
        $to_date = date('Y-m-d', strtotime($t));
        $dtt   = new DateTime($to_date);

        $from=$dtf->getTimestamp();
        $to=$dtt->getTimestamp();

          //$serviceAccount = ServiceAccount::fromJsonFile($d->jsonpath);
          $firebase = (new Factory)
          ->withServiceAccount($device->jsonpath)
          ->withDatabaseUri($device->url)
          ->createDatabase();
          $database = $firebase;
          
          $data=$database->getReference('/')
          ->orderByKey()
          ->startAt("$from")
          ->endBefore("$to")
          ->getSnapshot();
      
          $data=$data->getValue();
          $xvalues=[];
          $yvalues=[];

          $sensor = Sensor::find($sensor_id);
        
          foreach($data as $key=>$val){                       
              $xvalues[] = [$key*1000 ,(float)$val[$sensor->name]];
              //$yvalues[] = $val[$sensor->name];                                    
          }
          /* for($i=0;$i<=count($data);$i = $i+$diff){
              $xvalues[] = date('h:i:s',key($data[$i]));
              $yvalues[] = $data[$i];
          } */

          $out['type'] = 'success';
          $out['xvalue'] = $xvalues;
          //$out['yvalue'] = $yvalues;
          $out['max'] = $sensor->max;
          $out['min'] = $sensor->min;
          $out['device_name'] = $device->device_name;
          if($sensor->name == 'V'){
            $out['stepsize'] = 1;
          }elseif($sensor->name == 'I'){
            $out['stepsize'] = 10;
          }

          //dd($out);
      }else{
        $out['type'] = 'error';
        $out['message'] = 'error occured';
      }
      return json_encode($out);
    }
}
