<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Device;
use App\Models\Sensor;
use Auth;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use DateTime;

class TableDataController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');        
    }

    public function index(){
        $user_id =  Auth::user()->id;
        $devices = Device::where('user_id',$user_id)->get();
        return view('Data Table.data_table',compact('devices'));
    }

    public function getdata(Request $request){
        $user_id =  Auth::user()->id;
        $device_id = $request->id;
        $date = $request->date;

        $date = explode('-',$date);
        $f = str_replace('/', '-', $date[0]);
        $from_date = date('Y-m-d', strtotime($f));
        $dtf   = new DateTime($from_date);

        $t = str_replace('/', '-', $date[1]);
        $to_date = date('Y-m-d', strtotime($t));
        $dtt   = new DateTime($to_date);

        $from=$dtf->getTimestamp();
        $to=$dtt->getTimestamp();

        
        $device = Device::where('user_id',$user_id)->where('id',$device_id)->first();
        $firebase = (new Factory)
        ->withServiceAccount($device->jsonpath)
        ->withDatabaseUri($device->url)
        ->createDatabase();
        $database = $firebase;
        
        $data=$database->getReference('/')
            ->orderByKey()
            ->startAt("$from")
            ->endBefore("$to")
            ->getSnapshot();


        $data=$data->getValue();
        

        foreach($data as $key=>$data){
          if($key >= $from && $key <= $to){
            $data['date']=date('H:i:s | d-m-Y',$key);
            $k[]=$data;
          }
        }

        $j='';
        $i=1;

        ob_start();
        if(isset($k) && count($k)>0){

            echo '<thead class="cf text-center">
                <tr>
                    <th>No</th>';
                    foreach($k as $data){
                        foreach($data as $key=>$d){
                            echo '<th>'.$key.'</th>';
                        }
                        break;
                    }
                    echo '</tr>
            </thead>
            <tbody class="text-center">';
            foreach($k as $key=>$dj){
                echo '<tr>
                <td>'.$i.'</td>';
                foreach($dj as $key=>$h){
                echo '<td>'.$h.'</td>';
                }     
            '</tr>';
            $i++; } 
            echo '</tbody>';
        }

        $table_row = ob_get_contents();
        $table_row = str_replace("\n", '', $table_row);
        ob_clean();
        
        $out['type']      = 'success';
        $out['table_row'] = $table_row;
        echo json_encode($out);
        die;
         
    }
}

?>