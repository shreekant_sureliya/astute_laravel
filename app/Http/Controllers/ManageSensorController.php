<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Complaint;
use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use App\Models\Sensor;
use Auth;
use Illuminate\Support\Facades\Hash;

class ManageSensorController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
      $data = Sensor::paginate(10);
      // $data=Complaint::where('cpf', '=', 100)->count();
      return view('sensor.sensor_list',compact('data'));
      // return view('home');
    }

    public function create(){
      return view('sensor.sensor_create');
    }

    public function edit($id){
      if(isset($id)){
        $sensor = Sensor::find($id);
        return view('sensor.sensor_edit',compact('sensor'));
      }      
    }

    public function update($id,Request $request){      
      $request->validate([
        'name' => 'required|unique:users,name,'.$id,        
        'max' => 'required',        
        'min' => 'required',        
      ],[
        'name.required' => 'This Name Field is required',
        'max.required' => 'Please enter Max value',
        'min.required' => 'Please enter Min value',
      ]);

      $sensor = Sensor::find($id);
      $sensor->name = $request->name;
      $sensor->max = $request->max;
      $sensor->min = $request->min;
      $sensor->save();
      $request->session()->flash('message', 'Sensor Update successfully');
      return redirect()->route('sensor-manage');
    }

    public function save(Request $request){      
      $request->validate([
        'name' => 'required|unique:users',  
        'max' => 'required',        
        'min' => 'required',        
      ],[
        'name.required' => 'This Name Field is required',    
        'max.required' => 'Please enter Max value',
        'min.required' => 'Please enter Min value',    
      ]);

      $sensor = new Sensor;
      $sensor->name = $request->name;
      $sensor->max = $request->max;
      $sensor->min = $request->min;
      $sensor->save();

      $request->session()->flash('message', 'Sensor Add successfully');
      return redirect()->route('sensor-manage');
    }

    public function active_sensor(Request $request){

      $id = $request->id;
      $is_active = $request->is_active;

      if(isset($id) && isset($is_active)){
        $sensor = Sensor::find($id);
        $sensor->is_active = $is_active == 'true' ? 1 : 0;
        $sensor->save();
    
        $out['type'] = 'success';
      }else{
        $out['type'] = 'error';
      }
      echo json_encode($out);
      die;

    }
}
