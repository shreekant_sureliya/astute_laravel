<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Complaint;
use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use App\Models\Device;
use App\Models\Sensor;
use Auth;
use Illuminate\Support\Facades\Hash;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Contract\Database;


class ManageDeviceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Database $database)
    {
      $this->middleware('auth');
      $this->database = $database;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
      $data = Device::paginate(10);
      // $data=Complaint::where('cpf', '=', 100)->count();
      return view('device.device_list',compact('data'));
      // return view('home');
    }

    public function create(){
      $user = User::whereHas(
        'roles', function($q){
            $q->where('name', 'user');
        }
      )->get();
      $sensor = Sensor::get();
      return view('device.device_create',compact('user','sensor'));
    }

    public function edit($id){
      if(isset($id)){
        $user = User::get();
        $sensor = Sensor::active()->get();
        $device = Device::find($id);
        return view('device.device_edit',compact('device','user','sensor'));
      }      
    }

    public function update($id,Request $request){   
      $request->validate([
        'device_id' => 'required|unique:devices,device_id,'.$id,
        'imei_no' => 'required|unique:devices,IMEI_no,'.$id,
        'device_name' => 'required|unique:devices,device_name,'.$id,
        'user_id' => 'required',
        'url' => 'required',
        'gmail' => 'required',
        'password' => 'required',
        'sensor_id' => 'required',
        'gmail' => 'required|unique:devices,gmail,'.$id,
      ],[
        'password.required' => 'This Password Field is required',
        'email.required' => 'This Email Field is required',
      ]);
      
      $device = Device::find($id);
      $device->device_id = $request->device_id;
      $device->IMEI_no = $request->imei_no;
      $device->device_name = $request->device_name;
      $device->user_id = $request->user_id;
      $device->url = $request->url;
      $device->gmail = $request->gmail;
      $device->password = $request->password;

      if($request->hasfile('file')){

        $image_name = $request->file('file')->getClientOriginalName();
        $destinationPath = public_path('firebase_json');
        $request->file('file')->move($destinationPath,$image_name);
        $device->json_file = $image_name;

      }      
      $device->sensor = json_encode($request->sensor_id);
      $device->save();

    
      //firebase update data
      $updates = [
        $device->IMEI_no => [
          'device_id' => $device->device_id,
          'url' => $device->url
        ],
      ];
    
      $this->database->getReference('/')
       ->update($updates);


      $request->session()->flash('message', 'Device Update successfully');
      return redirect()->route('device-manage');
    }

    public function save(Request $request){      
      $request->validate([
        'device_id' => 'required|unique:devices',
        'imei_no' => 'required|unique:devices',
        'device_name' => 'required|unique:devices',
        'user_id' => 'required',
        'url' => 'required',
        'gmail' => 'required',
        'password' => 'required',
        'file' => 'required',
        'sensor_id' => 'required',
      ]);

      $device_id = $request->device_id;
      $device_id=strtoupper($device_id);
      $device_imei = $request->imei_no;
      $device_name = $request->device_name;
      $url = $request->url;
      $user_id = $request->user_id;
      $gmail = $request->gmail;
      $password = $request->password;
      $sensor_id = $request->sensor_id;

      $device = new Device;
      $device->device_id = $device_id;
      $device->IMEI_no = $device_imei;
      $device->device_name = $device_name;
      $device->user_id = $user_id;
      $device->url = $url;
      $device->gmail = $gmail;
      $device->password = $password;

      if($request->hasfile('file')){

        $image_name=$request->file('file')->getClientOriginalName();
        $destinationPath = public_path('firebase_json');
        $request->file('file')->move($destinationPath,$image_name);
        $device->json_file = $image_name;

      }      
      $device->sensor = json_encode($sensor_id);
      $device->save();
      
      /* $data=$this->database->getReference('/')
      ->push([
        "323" => [
            'IMEI' => "1234555555555557",
            'url' => "hhttp://newdata.com",
        ],
      ]); */

      //firebase add data
      $updates = [
        $device->IMEI_no => [
          'device_id' => $device->device_id,
          'url' => $device->url
        ],
      ];
    
      $this->database->getReference('/')
       ->update($updates);

      $request->session()->flash('message', 'Device Add successfully');
      return redirect()->route('device-manage');
    }

}
