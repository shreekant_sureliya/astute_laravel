<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Device extends Model
{
    use HasFactory;
    protected $appends = ['jsonpath'];


    public function user()
    {
        return $this->belongsTo(User::class,'user_id','id');
    }

    public function getjsonpathAttribute(){
        $main_root_path = $_SERVER['DOCUMENT_ROOT'];
        return $main_root_path.'/firebase_json/'.$this->json_file;
      }
}
