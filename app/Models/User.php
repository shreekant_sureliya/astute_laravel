<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Laratrust\Traits\LaratrustUserTrait;
use Illuminate\Foundation\Auth\Access\Authorizable;

class User extends Authenticatable
{   

    use LaratrustUserTrait;
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public static function create_user(){
        echo "hello shree";
         // $user=new User;
         // $user->cpf=1;
         // $user->password=bcrypt('12345678');
         // $user->save();

    }


    public static function upload_csv(){
        $filename=public_path().'/data/mp.csv';
        $file = fopen($filename, "r");
        $data_arr = array(); // Read through the file and store the contents as an array
        $i = 0;
        //Read the contents of the uploaded file 
        while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
        $num = count($filedata);
        // echo $filedata;
        // Skip first row (Remove below comment if you want to skip the first row)
        if ($i == 0) {
            $i++;
            continue;
            }
            for ($c = 0; $c < $num; $c++) {
                // echo $filedata[$c];
                $data_arr[$i][] = $filedata[$c];
            }
            $i++;
        }

        fclose($file);
        
        foreach ($data_arr as $count => $value) {
            

            $user=new User;
            $user->cpf=$value[0];
            $user->name=$value[1];
            $user->designation=$value[2];
            $user->ongc_code=$value[3];
            $user->level=$value[4];
            $user->class=$value[5];
            $user->discipline=$value[6];
            $user->sub_disicpline=$value[7];
            $user->personal_area=$value[8];
            $user->location=$value[9];
            $user->region=$value[10];
            $user->org_unit=$value[11];
            $user->parent_unit=$value[12];
            $user->postion=$value[13];
            $user->religion=$value[14];
            $user->category=$value[15];
            $user->gender=$value[16];
            $user->qualification=$value[17];
            $user->qual_level=$value[18];
            $user->status_dom=$value[19];
            $user->dob=$value[20];
            $user->date_of_join=$value[21];
            $user->date_of_post=$value[22];
            $user->eff_date_prom=$value[23];
            $user->date_of_join_per_area=$value[24];
            $user->date_of_join_position=$value[25];
            $user->date_of_retirement=$value[26];
            $user->exservice_man=$value[27];
            $user->handicap=$value[28];
            $user->native=$value[29];
            $user->tnt_text=$value[30];
            $user->regular_term_base=$value[31];
            $user->state_code=$value[32];
            $user->relat=$value[33];
            $user->desig_code=$value[34];
            $user->r_p_cd=$value[35];
            $user->verison=$value[36];
            $user->religion_denom=$value[37];
            $user->tech_non_tech=$value[38];
            $user->disc_cd=$value[39];
            $user->sub_disc_cd=$value[40];
            $user->zqualcode=$value[41];
            $user->mpi_1=$value[42];
            $user->mpi_2=$value[43];
            $user->birth_year=$value[44];
            $user->org_unit_number=$value[45];
            $user->position_number=$value[46];
            $user->job_id=$value[47];
            $user->administator=$value[48];
            $user->mobile_no=$value[49];
        
            $dob=$value[20];
            $temp=explode("/", $dob);
            $month=$temp[0];
            $day=$temp[1];
            $yr=$temp[2];
            

            if(strlen($day)!=2){
                $day="0".$day;
            }
            if(strlen($month)!=2){
                $month="0".$month;
            }

            $pwd=$day.$month.$yr;
            
            $user->password=bcrypt($pwd);
            $user->save();

            echo $value[0];

        }
       
    }
}
