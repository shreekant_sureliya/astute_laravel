@extends('adminlte::page',['sidebar' => true])
@section('title', 'Data')

@section('content_header')
@stop

@section('content')
<div class="row">
  <div class="col-lg-12">
  <header class="m-2"> Data </header>     
    <section class="card">
    <div class="card-header">
      <div class="form-row">
        <div class="form-group col col-md-5">
        <label for="inputEmail4">Select Device</label>
        <select class="form-control form-control-sm" id="device">
          <?php foreach($devices as $device){ ?>
              <option value="<?php echo $device->id ?>"><?php echo $device->device_id ?></option>
          <?php } ?>
        </select>
        </div>
        <div class="form-group col col-md-4">
        <label for="inputEmail4">Date  : </label>
          <input id="date" type="text" name="date" class="form-control form-control-sm datepicker_range" autocomplete="off">
        </div>
        <div class="form-group col col-md-2" style="padding-top: 33px;">
          <button class="btn-primary btn-sm" id="btn_go">Go</button>
          <button class="btn-info btn-sm" id="export">export</button>
        </div>
      </div>
	</div>
    {{-- <img id="loading" src={{ URL::to("loading/loading.gif") }} >--}}
      <div class="card-body"> 
				<section id="flip-scroll">
					<table class="table" id="datatable" style="display:none">
						
					</table>
				</section>
        <svg id="loader" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="margin:auto;background:#fff;display:block;" width="197px" height="197px" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid">
          <circle cx="50" cy="50" r="0" fill="none" stroke="#e90c59" stroke-width="1">
            <animate attributeName="r" repeatCount="indefinite" dur="1.7543859649122806s" values="0;51" keyTimes="0;1" keySplines="0 0.2 0.8 1" calcMode="spline" begin="0s"></animate>
            <animate attributeName="opacity" repeatCount="indefinite" dur="1.7543859649122806s" values="1;0" keyTimes="0;1" keySplines="0.2 0 0.8 1" calcMode="spline" begin="0s"></animate>
          </circle><circle cx="50" cy="50" r="0" fill="none" stroke="#46dff0" stroke-width="1">
            <animate attributeName="r" repeatCount="indefinite" dur="1.7543859649122806s" values="0;51" keyTimes="0;1" keySplines="0 0.2 0.8 1" calcMode="spline" begin="-0.8771929824561403s"></animate>
            <animate attributeName="opacity" repeatCount="indefinite" dur="1.7543859649122806s" values="1;0" keyTimes="0;1" keySplines="0.2 0 0.8 1" calcMode="spline" begin="-0.8771929824561403s"></animate>
          </circle>
        </svg>
			</div>
    </section>
  </div>
</div>


@section('adminlte_js')
<script>


$(document).ready(function(){
  $('#loader').hide();
});

$(document).ajaxStart(function () {
  $('#loader').show();
  $('#container').hide();
});

$(document).ajaxComplete(function () {
  $('#loader').hide();
  $('#container').show();
});

$(function() {
  $('#date').daterangepicker();
});

$('#date').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
  });

    


@if(Session::has('message'))
var Toast = Swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 2000,
  timerProgressBar: true,
  onOpen: function(toast) {
    toast.addEventListener('mouseenter', Swal.stopTimer)
    toast.addEventListener('mouseleave', Swal.resumeTimer)
  }
});

Toast.fire({
  icon: 'success',
  title: '{{ Session::get('message') }}'
});
@endif

$.ajaxSetup({
   headers: {
     'X-CSRF-TOKEN': "{{ csrf_token() }}",
   }
});

$(document).on('click','#btn_go',function(){
        var id = $('#device').val();
        var date = $('#date').val();
        $.ajax({
            
            url : "{{ route('get-data') }}",
            type: "POST",
            data : {
              id : id,
              date : date,
            },
            success: function(data, textStatus, jqXHR)
            {
                data = JSON.parse(data);
                if(data.type == 'error'){
                        toastr.error(data.message);
                }
                if(data.type == 'success'){
                    $('#datatable').html(data.table_row);
                }

            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                        
            }
        });
    });

    let options = {
      "separator": ",",
      "newline": "\n",
      "quoteFields": true,
      "excludeColumns": "",
      "excludeRows": "",
      "trimContent": true,
      "filename": "table.csv",
      "appendTo": "#output"
    }

$(document).on('click','#export',function(){
    // $('#datatable').csvExport({
    //     title:'tbl_1'
    // });
    $('#datatable').table2csv({
        file_name: 'data.csv'
    });

});

$( document ).ajaxStart(function() {
    //$( "#loading" ).show();
    $('#datatable').hide();
 });

 $( document ).ajaxComplete(function() {
    //$( "#loading" ).hide();
    $('#datatable').show();

 });

</script>
@stop
@stop
