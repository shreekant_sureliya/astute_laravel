@extends('adminlte::page',['sidebar' => true])
@section('title', 'Device-create')

@section('content_header')
@stop

@section('content')

@if(session()->has('success'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
  {{ session()->get('success') }}
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif

<div class="row p-2">
    <h5>Create Device</h5>
</div>
<div class="row p-3 card">
    <div class="card-body">
        <form class="form-horizontal" action="{{ route('device.save') }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group row">
                <label for="device_id" class="col-sm-2 control-label">Device Id</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control @error('device_id') is-invalid @enderror" name="device_id" value="{{old('device_id')}}" id="device_id" placeholder="Enter Device Id" >
                    @error('device_id')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>                
            </div>
            <div class="form-group row">
                <label for="imei_no" class="col-sm-2 control-label">Device IMEI</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control @error('imei_no') is-invalid @enderror" name="imei_no" value="{{old('imei_no')}}" id="imei_no" placeholder="Enter Device IMEI no" >
                    @error('imei_no')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                </div>
                
            </div>
            <div class="form-group row">
                <label for="device_name" class="col-sm-2 control-label">Device Name</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control @error('device_name') is-invalid @enderror" name="device_name" value="{{old('device_name')}}" id="device_name" placeholder="Enter Device Name" >
                
                    @error('device_name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                </div>
                
            </div>
            <div class="form-group row">
                <label for="device_id" class="col-sm-2 control-label">URL</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control @error('url') is-invalid @enderror" name="url" value="{{old('url')}}" id="url" placeholder="Enter URL" >
                
                    @error('url')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                </div>
                
            </div>
            <div class="form-group row">
                <label for="device_id" class="col-sm-2 control-label">Select User</label>
                <div class="col-sm-10">
                    <select name="user_id" id="user_id" class="form-control @error('user_id') is-invalid @enderror"  >
                        <option value="">Please Select User</option>
                        @foreach($user as $user)
                            <option value="<?php echo $user->id?>"><?php echo $user->name?></option>
                        @endforeach

                        @error('user_id')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                    </select>
                </div>
                
            </div>
            <div class="form-group row">
                <label for="device_id" class="col-sm-2 control-label">Select Sensor</label>
                <div class="col-sm-10">
                    <select name="sensor_id[]" id="sensor_id" class="form-control @error('sensor_id') is-invalid @enderror"   multiple>
                        <option value="">Please Select Sensor</option>
                        @foreach($sensor as $sensor)
                            <option value="<?php echo $sensor->id?>"><?php echo $sensor->name?></option>
                        @endforeach

                        @error('sensor_id')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                    </select>
                </div>
                
            </div>
            <div class="form-group row">
                <label for="gmail" class="col-sm-2 control-label">Gmail</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control @error('gmail') is-invalid @enderror" name="gmail"  value="{{old('gmail')}}" id="gmail" placeholder="Enter Gmail" >
                @error('gmail')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                </div>

            </div>
            <div class="form-group row">
                <label for="password" class="col-sm-2 control-label">Password</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control @error('password') is-invalid @enderror" name="password" value="{{old('password')}}" id="password" placeholder="Enter Password" >
                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                </div>

            </div>
            <div class="form-group row">
                <label for="password" class="col-sm-2 control-label">File Upload</label>
                <div class="col-sm-10">
                    <input type="file" class="@error('file') is-invalid @enderror" name="file" id="file">
                @error('file')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                </div>

            </div>
            <hr>
            <div class="box-footer">
            <button type="submit" class="btn btn-sm btn-primary">Add Device</button>
            <a href="{{route('device-manage')}}" class="btn btn-sm btn-default">Cancel</a>
            </div>
        </form>
    </div>
</div>

@section('adminlte_js')
<script>
@if(Session::has('message'))
  var Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 2000,
    timerProgressBar: true,
    onOpen: function(toast) {
      toast.addEventListener('mouseenter', Swal.stopTimer)
      toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
  });

  Toast.fire({
    icon: 'success',
    title: '{{ Session::get('message') }}'
  });
@endif
</script>
@stop
@stop
