@extends('adminlte::page',['sidebar' => true])
@section('title', 'Device-Edit')

@section('content_header')
@stop

@section('content')

@if(session()->has('success'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
  {{ session()->get('success') }}
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif

@if (count($errors) > 0)
  <div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
       @foreach ($errors->all() as $error)
         <li>{{ $error }}</li>
       @endforeach
    </ul>
  </div>
@endif

<div class="row p-2">
    <h5>Edit Device</h5>
</div>
<div class="row p-3 card">
    <div class="card-body">
        <form action="{{ route('device.update',['id'=>$device->id]) }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group row">
                <label for="device_id" class="col-sm-2 control-label">Device Id</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control @error('device_id') is-invalid @enderror" name="device_id" value="{{$device->device_id}}" id="device_id" placeholder="Enter Device Id" required>
                @error('device_id')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                </div>

            </div>
            <div class="form-group row">
                <label for="imei_no" class="col-sm-2 control-label">Device IMEI</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control @error('imei_no') is-invalid @enderror" name="imei_no" value="{{$device->IMEI_no}}" id="imei_no" placeholder="Enter Device IMEI no" required>
                @error('imei_no')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                </div>

            </div>
            <div class="form-group row">
                <label for="device_name" class="col-sm-2 control-label">Device Name</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control @error('device_name') is-invalid @enderror" name="device_name" value="{{$device->device_name}}" id="device_name" placeholder="Enter Device Name" required>
                @error('device_name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                </div>

            </div>
            <div class="form-group row">
                <label for="device_id" class="col-sm-2 control-label">URL</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control @error('url') is-invalid @enderror" name="url" id="url" value="{{$device->url}}" placeholder="Enter URL" required>
                @error('url')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                </div>

            </div>
            <div class="form-group row">
                <label for="device_id" class="col-sm-2 control-label">Select User</label>
                <div class="col-sm-10">
                    <select name="user_id" id="user_id" class="form-control"  required>
                        <option value="">Please Select User</option>
                        @foreach($user as $user)
                          @if($user->id == $device->user->id)
                            <option value="{{$user->id}}" selected>{{$user->name}}</option>
                          @else
                            <option value="{{$user->id}}">{{$user->name}}</option>
                          @endif
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="device_id" class="col-sm-2 control-label">Select Sensor</label>
                <div class="col-sm-10">
                    <select name="sensor_id[]" id="sensor_id" class="form-control"  required multiple>
                        <option value="">Please Select Sensor</option>
                            @foreach($sensor as $sens)
                                @if(in_array($sens->id,json_decode($device->sensor)))
                                    <option value="{{$sens->id}}" selected>{{$sens->name}}</option>
                                @else
                                    <option value="{{$sens->id}}" >{{$sens->name}}</option>
                                @endif
                        @endforeach
				    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="gmail" class="col-sm-2 control-label">Gmail</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control @error('gmail') is-invalid @enderror" name="gmail" id="gmail" value="{{$device->gmail}}" placeholder="Enter Gmail" required>
                @error('gmail')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                </div>
            </div>
            <div class="form-group row">
                <label for="password" class="col-sm-2 control-label">Password</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control @error('password') is-invalid @enderror" name="password" value="{{$device->password}}" id="password" placeholder="Enter Password" required>
                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                </div>
            </div>
            @if($device->json_file != null)
            <div class="form-group row">
                <div class="offset-md-2 col-sm-10">
                    <label class="col-sm-10 control-label">File Already Selected</label>
                </div>
            </div>
            @endif
            <div class="form-group row">
                <label for="password" class="col-sm-2 control-label">File Upload</label>
                <div class="col-sm-10">
                    <input type="file" name="file" id="file">
                </div>
            </div>
            <hr>
            <div class="box-footer">
                <button type="submit" class="btn btn-sm btn-primary">Update Device</button>
                <a href="{{ route('device-manage')}}" class="btn btn-sm btn-default">Cancel</a>
            </div>
        </form>
    </div>
</div>

@section('adminlte_js')
<script>
@if(Session::has('message'))
var Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 2000,
        timerProgressBar: true,
        onOpen: function(toast) {
        toast.addEventListener('mouseenter', Swal.stopTimer)
        toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    });

    Toast.fire({
        icon: 'success',
        title: '{{ Session::get('message') }}'
    });
@endif
</script>
@stop
@stop
