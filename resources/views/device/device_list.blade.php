@extends('adminlte::page',['sidebar' => true])
@section('title', 'Device')

@section('content_header')
@stop

@section('content')
  
<div class="row p-3">
    <div class="col-6">
        <h5>Device List</h5>
    </div>

    <div class="col-6 text-right">
      <a href="{{route('device.create')}}" class="btn btn-xs btn-primary">Create</a>
    </div>

    <div class="card row-12 w-100 p-3">
      <table class="table table-striped table-sm">
        <thead>
            <tr>
                <th>#</th>
								<th>Device Id</th>
								<th>Device IMEI</th>
								<th>Device Name</th>
								<th>User Name</th>
								<th>Url</th>
								<th>Action</th>
            </tr>           
        </thead>
        <tbody>
            @if(!empty($data) && $data->count() > 0)
                @php $i=0 @endphp
                @foreach($data as $value)
                    <tr>
                      <td>{{++$i}}</td>
                      <td>{{ $value->device_id }}</td>
                      <td>{{ $value->IMEI_no }}</td>
                      <td>{{ $value->device_name }}</td>
                      <td>{{ $value->user->name }}</td>
                      <td>{{ $value->url }}</td>
                      <td>
                      <a href="{{ route('device.edit',['id'=>$value->id]) }}" class="btn btn-primary btn-xs"><i class="fas fa-fw fa-edit pr-4"></i>Edit</a>
                      <a href="#" class="btn btn-danger btn-xs">Delete</a>
                      </td>
                    </tr>
                @endforeach
            @else
              <tr>
                <td class="text-center" colspan="5">There are no Any Device.</td>
              </tr>
            @endif
        </tbody>
    </table>
    </div>

    {!! $data->links('pagination::bootstrap-4') !!}
    
    

</div>

@section('adminlte_js')
<script>
@if(Session::has('message'))
    var Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 2000,
      timerProgressBar: true,
      onOpen: function(toast) {
        toast.addEventListener('mouseenter', Swal.stopTimer)
        toast.addEventListener('mouseleave', Swal.resumeTimer)
      }
    });

    Toast.fire({
      icon: 'success',
      title: '{{ Session::get('message') }}'
    });
@endif
</script>
@stop
@stop
