@extends('adminlte::page',['sidebar' => true])
@section('title', 'User')

@section('content_header')
@stop

@section('content')
  
<div class="row p-3">
    <div class="col-6">
        <h5>User List</h5>
    </div>
    <div class="col-6 text-right">
      <a href="{{route('user.create')}}" class="btn btn-xs btn-primary">Create</a>
    </div>
    <div class="card row-12 w-100 p-3">
      <table class="table table-striped table-sm">
        <thead>
            <tr>
              <th>#</th>
              <th>Email</th>
              <th>User Name</th>
              <th>Status</th>
              <th>Action</th>
            </tr>           
        </thead>
        <tbody>
            @if(!empty($data) && $data->count() > 0)
                @php $i=0 @endphp
                @foreach($data as $value)
                    <tr>
                        <td>{{ ++$i }}</td>
                        <td>{{ $value->email }}</td>
                        <td>{{ $value->name}}</td>
                        <td>
                          <div class="custom-control custom-switch">
                            <input type="checkbox" class="switch custom-control-input" id="{{$value->id}}" {{$value->is_active == 1 ? 'checked' : ''}}>
                            <label class="custom-control-label" for="{{$value->id}}"></label>
                          </div>
                        </td>
                        <td>                         
                          <a href="{{ route('user.edit',['id'=>$value->id])}}" class="btn btn-xs btn-primary" role="button" ><i class="fas fa-fw fa-edit pr-4"></i>Edit</a>
                        </td>
                    </tr>
                @endforeach
            @else
              <tr>
                <td colspan="10">There are no User.</td>
              </tr>
            @endif
        </tbody>
    </table>
    </div>

    {!! $data->links('pagination::bootstrap-4') !!}
    
    

</div>

@section('adminlte_js')
<script>
@if(Session::has('message'))
var Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 2000,
    timerProgressBar: true,
    onOpen: function(toast) {
      toast.addEventListener('mouseenter', Swal.stopTimer)
      toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
  });

  Toast.fire({
    icon: 'success',
    title: '{{ Session::get('message') }}'
  });
@endif

$(document).on('change','.switch',function(){
  //console.log(this.id);
  var is_active = $(this).is(":checked"); 
  var id = $(this).attr('id');
  $.ajax({
      url : "{{ route('active-user') }}",
      type: "POST",
      data : {
        id : id,
        is_active : is_active,
        _token : "{{csrf_token()}}"
      },
      success: function(data, textStatus, jqXHR)
      {
          data = JSON.parse(data);
          if(data.type == 'error'){
            var Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 2000,
              timerProgressBar: true,
              onOpen: function(toast) {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
              }
            });

            Toast.fire({
              icon: 'error',
              title: 'something went wrong'
            });
          }
          if(data.type == 'success'){
            var Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 2000,
              timerProgressBar: true,
              onOpen: function(toast) {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
              }
            });

            Toast.fire({
              icon: 'success',
              title: 'user status changed'
            });
          }

      },
  });
});
</script>
@stop
@stop
