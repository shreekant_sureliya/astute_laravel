@extends('adminlte::page',['sidebar' => true])
@section('title', 'User-create')

@section('content_header')
@stop

@section('content')

@if(session()->has('success'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
  {{ session()->get('success') }}
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif

<div class="row p-2">
    <h5>Create User</h5>
</div>
<div class="row p-3 card">
    <div class="card-body">
    <form class="form-horizontal"  action="{{route('user.save')}}" method="post">
        @csrf
            <div class="form-group row">
            <label for="device_id" class="col-sm-2 control-label">Name</label>
                <div class="col-sm-10">
                    <input name="name" type="text" class="form-control   @error('name') is-invalid @enderror" placeholder="Enter Name">
                </div>
                @error('name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>

            <div class="form-group row">
            <label for="device_id" class="col-sm-2 control-label">Email</label>
                <div class="col-sm-10">
                    <input name="email" type="text"class="form-control   @error('email') is-invalid @enderror" placeholder="Enter Email">
                </div>
                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>

            <div class="form-group row">
                <label for="device_id" class="col-sm-2 control-label">Password</label>
                <div class="col-sm-10">
                    <input name="password" type="text"class="form-control   @error('password') is-invalid @enderror" placeholder="Enter Password">
                </div>
                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            
            <div class="form-group row">
                <label for="device_id" class="col-sm-2 control-label">Contact No. </label>
                <div class="col-sm-10">
                    <input name="contact_no" type="text" class="form-control  " placeholder="Enter Contact No.">
                </div>
            </div>
           
            <div class="box-footer">
                <button type="submit" class="btn btn-sm btn-primary">Save</button>
                <a href="{{route('user-manage')}}" class="btn btn-sm btn-default">Cancel</a>
            </div>
        </form>
    </div>
</div>

@section('adminlte_js')
<script>
@if(Session::has('message'))
var Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 2000,
        timerProgressBar: true,
        onOpen: function(toast) {
        toast.addEventListener('mouseenter', Swal.stopTimer)
        toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    });

    Toast.fire({
        icon: 'success',
        title: '{{ Session::get('message') }}'
    });
@endif
</script>
@stop
@stop
