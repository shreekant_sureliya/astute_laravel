@extends('adminlte::page',['sidebar' => true])
@section('title', 'Graph')

@section('content_header')
@stop
<style>
.chartWrapper {
  position: relative;
}

.chartWrapper > canvas {
  position: absolute;
  left: 0;
  top: 0;
  pointer-events: none;
}

.chartAreaWrapper {
  width: 1000px;
  overflow-x: scroll;
}




</style>


@section('content')

<div class="row">
  <div class="col-lg-12">
  <header class="m-2"> Graph </header>     
    <section class="card">
    <div class="card-header">
      <div class="form-row">
        <div class="form-group col col-md-3">
          <label for="inputEmail4">Select Device</label>
          <select class="form-control form-control-sm" id="device" name="device"> 
            <?php foreach($devices as $device){ ?>
                <option value="<?php echo $device->id ?>"><?php echo $device->device_name ?></option>
            <?php } ?>
          </select>
        </div>
        <div class="form-group col col-md-2">
          <label for="inputEmail4">Select Sensor</label>
          <select class="form-control form-control-sm" id="sensor" name="sensor">
            <?php foreach($sensors as $sensor){ ?>
                <option value="<?php echo $sensor->id ?>"><?php echo $sensor->name ?></option>
            <?php } ?>
          </select>
        </div>
        <div class="form-group col col-md-3">
        <label for="inputEmail4">Date</label>
          <input id="date" type="text" name="date" class="form-control form-control-sm datepicker_range" autocomplete="off">
        </div>
        @php
        $arr = array();
        $arr['second']='second';
        $arr['minute']='minute';
        $arr['hour']='hour';

        @endphp
        <div class="form-group col col-md-3">
          <label for="inputEmail4">Select Type</label>
          <select class="form-control form-control-sm" id="type" name="type">
            <?php foreach($arr as $key=>$ar){ ?>
                <option value="<?php echo $key ?>"><?php echo $ar ?></option>
            <?php } ?>
          </select>
        </div>
        <div class="form-group col col-md-1" style="padding-top: 30px;">
          <button class="btn-primary btn-sm" id="btn_go">Go</button>
        </div>
      </div>

    <div class="chartWrapper">
      <div class="chartAreaWrapper">
        <div class="chartAreaWrapper2">
            <canvas id="myChart" height="300" width="1200"></canvas>
        </div>
      </div>
    <canvas id="myChartAxis" height="300" width="0"></canvas>
  </div>

	</div>

@section('adminlte_js')
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/chart.js"></script>
<script>

$(function() {
  $("#graph_section").hide();
  $('#date').daterangepicker();
});

$('#date').on('apply.daterangepicker', function(ev, picker) {
  $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
});

$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': "{{ csrf_token() }}",
  }
});

$(document).on('click','#btn_go',function(){
  var device_id = $('#device').val();
  var sensor_id = $('#sensor').val();
  var type = $('#type').val();
  var date = $('#date').val();
  $.ajax({
      
      url : "{{ route('get_graph_data') }}",
      type: "POST",
      data : {
        device_id : device_id,
        sensor_id : sensor_id,
        type : type,
        date : date,
      },
      success: function(data, textStatus, jqXHR)
      {
          data = JSON.parse(data);
          if(data.type == 'error'){
            toastr.error(data.message);
          }
          if(data.type == 'success'){
            set_data(data.xvalue,data.yvalue,data.min,data.max,data.stepsize,data.device_name);
            $("#graph_section").show();
          }
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
                  
      }
  });
});


function set_data(x,y,min_val,max_val,stepsize_val,device_name){
  
var ctx = document.getElementById("myChart").getContext("2d");
var gradient = ctx.createLinearGradient(0, 0, 0, 300);
                gradient.addColorStop(0,"#E3E1F9");   
                gradient.addColorStop(1,"#FBFBFF");
        var data = {
            labels: x ,
            datasets: [
                {
                    // backgroundColor: gradient,
                    // borderColor: "#696BFA",
                    // borderCapStyle: 'square'
                    // label: "My First dataset",
                    // fill: true,
                    // lineTension: 0.5,
                    // fillColor: "rgba(220,220,220,0.2)",
                    // strokeColor: "rgba(220,220,220,1)",
                    // pointColor: "rgba(220,220,220,1)",
                    // pointStrokeColor: "#fff",
                    // pointHighlightFill: "#fff",
                    // pointHighlightStroke: "rgba(220,220,220,1)",
                    // data: y,

                    label:device_name,
                    fill: true,
                    lineTension: 0.5,
                    backgroundColor: gradient,
                    borderColor: "#696BFA", // The main line color
                    borderCapStyle: 'square',
                    pointBorderColor: "white",
                    pointBackgroundColor: "#696BFA",
                    pointBorderWidth: 1,
                    pointHoverRadius: 8,
                    pointHoverBackgroundColor: "#696BFA",
                    pointHoverBorderColor: "#696BFA",
                    pointHoverBorderWidth: 2,
                    pointRadius: 4,
                    pointHitRadius: 10,
                    data: y,
                    spanGaps: true,
                },                
            ]
        };      

    // render init block
    var rectangleSet = false;
    const myChart = new Chart(
      document.getElementById('myChart'),{
      type: 'line',
      data,
      options: {
        layout : {
          padding : {
            right : 18,
            bottom : 30,
          }
        },  
        scales: {
          x:{
            min : 0,
            max : 500,
            ticks: {
              fontSize: 12,
              display: true,           
            },
            grid: {
              display:false,
            }   
          },
          y: {            
            min : min_val,
            max : max_val,
            beginAtZero: true,
            ticks: {          
              fontSize: 12,
              display: true,     
              stepSize: stepsize_val,       
            }, 
            grid: {
              borderDash: [4, 4],
            }  
          }
        },
        animation:{
          /* onComplete : function(){
            console.log('on complete');
            const {ctx,canvas,chartArea : {left,right,top,bottom,width,height}} = myChart;

            if (!rectangleSet) {
                var scale = window.devicePixelRatio;                       

                var sourceCanvas = myChart.ctx.canvas;
                var copyWidth = width - 5000;
                var copyHeight = height + top ;

                ctx.scale(scale, scale);
                ctx.canvas.width = copyWidth * scale;
                ctx.canvas.height = copyHeight * scale;

                ctx.canvas.style.width = `${copyWidth}px`;
                ctx.canvas.style.height = `${copyHeight}px`;
                ctx.drawImage(sourceCanvas, 0, 0, copyWidth * scale, copyHeight * scale, 0, 0, copyWidth * scale, copyHeight * scale);

                var sourceCtx = sourceCanvas.getContext('2d');

                // Normalize coordinate system to use css pixels.

                sourceCtx.clearRect(0, 0, copyWidth * scale, copyHeight * scale);
                rectangleSet = true;
            }
          }, */
          /* onProgress: function () {
            console.log('on progress');
            const {ctx,canvas,chartArea : {left,right,top,bottom,width,height}} = myChart;

              if (rectangleSet === true) {
                  var copyWidth = width;
                  var copyHeight = height + top + 10;
                  ctx.clearRect(0, 0, copyWidth, copyHeight);
              }
          } */
        }
      },
    });
    var newwidth = $('.chartAreaWrapper2').width() + 500;
    $('.chartAreaWrapper2').width(newwidth);
}
</script>
@stop
@stop
