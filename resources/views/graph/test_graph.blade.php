@extends('adminlte::page',['sidebar' => true])
@section('title', 'Graph')

@section('content_header')
@stop
<style>
.chartWrapper {
    position: relative;
}

.chartWrapper > canvas {
    position: absolute;
    left: 0;
    top: 0;
    pointer-events:none;
}

.chartAreaWrapper {
    width: 600px;
    overflow-x: scroll;
}

</style>


@section('content')

<div class="row">
  <div class="col-lg-12">
  <header class="m-2"> Graph </header>     
    <section class="card">
    <div class="card-header">
      <div class="form-row">
        <div class="form-group col col-md-3">
          <label for="inputEmail4">Select Device</label>
          <select class="form-control form-control-sm" id="device" name="device"> 
            <?php foreach($devices as $device){ ?>
                <option value="<?php echo $device->id ?>"><?php echo $device->device_name ?></option>
            <?php } ?>
          </select>
        </div>
        <div class="form-group col col-md-2">
          <label for="inputEmail4">Select Sensor</label>
          <select class="form-control form-control-sm" id="sensor" name="sensor">
            <?php foreach($sensors as $sensor){ ?>
                <option value="<?php echo $sensor->id ?>"><?php echo $sensor->name ?></option>
            <?php } ?>
          </select>
        </div>
        <div class="form-group col col-md-3">
        <label for="inputEmail4">Date</label>
          <input id="date" type="text" name="date" class="form-control form-control-sm datepicker_range" autocomplete="off">
        </div>
        @php
        $arr = array();
        $arr['second']='second';
        $arr['minute']='minute';
        $arr['hour']='hour';

        @endphp
        <div class="form-group col col-md-3">
          <label for="inputEmail4">Select Type</label>
          <select class="form-control form-control-sm" id="type" name="type">
            <?php foreach($arr as $key=>$ar){ ?>
                <option value="<?php echo $key ?>"><?php echo $ar ?></option>
            <?php } ?>
          </select>
        </div>
        <div class="form-group col col-md-1" style="padding-top: 30px;">
          <button class="btn-primary btn-sm" id="btn_go">Go</button>
        </div>
      </div>


      <div class="chartWrapper">
        <div class="chartAreaWrapper">
            <canvas id="myChart" height="300" width="1200"></canvas>
        </div>
        <canvas id="myChartAxis" height="300" width="0"></canvas>
    </div>

	</div>

@section('adminlte_js')
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/chart.js"></script>
<script>

$(function() {
  $("#graph_section").hide();
  $('#date').daterangepicker();
});

$('#date').on('apply.daterangepicker', function(ev, picker) {
  $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
});

$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': "{{ csrf_token() }}",
  }
});

$(document).on('click','#btn_go',function(){
  var device_id = $('#device').val();
  var sensor_id = $('#sensor').val();
  var type = $('#type').val();
  var date = $('#date').val();
  $.ajax({
      
      url : "{{ route('get_graph_data') }}",
      type: "POST",
      data : {
        device_id : device_id,
        sensor_id : sensor_id,
        type : type,
        date : date,
      },
      success: function(data, textStatus, jqXHR)
      {
          data = JSON.parse(data);
          if(data.type == 'error'){
            toastr.error(data.message);
          }
          if(data.type == 'success'){
            set_data(data.xvalue,data.yvalue,data.min,data.max,data.stepsize);
            $("#graph_section").show();
          }
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
                  
      }
  });
});


function set_data(x,y,min_val,max_val,stepsize_val){
var ctx = document.getElementById("myChart").getContext("2d");

        var data = {
            labels: ["January", "February", "March", "April", "May", "June", "July","January", "February", "March", "April", "May", "June", "July"],
            datasets: [
                {
                    label: "My First dataset",
                    fillColor: "rgba(220,220,220,0.2)",
                    strokeColor: "rgba(220,220,220,1)",
                    pointColor: "rgba(220,220,220,1)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(220,220,220,1)",
                    data: [65, 59, 80, 81, 56, 55, 40, 65, 59, 80, 81, 56, 55, 40]
                },
                {
                    label: "My Second dataset",
                    fillColor: "rgba(151,187,205,0.2)",
                    strokeColor: "rgba(151,187,205,1)",
                    pointColor: "rgba(151,187,205,1)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(151,187,205,1)",
                    data: [28, 48, 40, 19, 86, 27, 90, 28, 48, 40, 19, 86, 27, 90]
                }
            ]
        };      

    // render init block
    const myChart = new Chart(
      document.getElementById('myChart'),{
      type: 'line',
      data,
      options: {
        layout : {
          padding : {
            right : 18,
            bottom : 30,
          }
        },  
        scales: {
          x:{
            ticks: {
              fontSize: 12,
              display: true,           
            },
            grid: {
                display:false,
              }   
          },
          y: {            
            beginAtZero: true,
            ticks: {          
              fontSize: 12,
              display: true,            
            }, 
            grid: {
                borderDash: [4, 4],
            }  
          }
        },
        animation:{
            onComplete : function(){
                console.log('complete');
                var sourceCanvas = myChart.ctx.canvas;
                //var copyWidth = myChart.scales.x.paddingScalePaddingLeft - 5;
                //var copyHeight = myChart.scale.endPoint + 5;
                var copyWidth = myChart.scales.x.paddingScalePaddingLeft - 5;
                var copyHeight = myChart.scale.endPoint + 5;
                ctx.canvas.width = copyWidth;
                ctx.drawImage(sourceCanvas, 0, 0, copyWidth, copyHeight, 0, 0, copyWidth, copyHeight);
            }
        }
      },
    });
}
</script>
@stop
@stop
