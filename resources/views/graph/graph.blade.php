@extends('adminlte::page',['sidebar' => true])
@section('title', 'Graph')

@section('content_header')
@stop
<style type="text/css">
      #container {
          height: 400px;
          min-width: 310px;
      }
    </style>
</style>
@section('content')
<body>
<div class="row">
  <div class="col-lg-12">
  <header class="m-2"> Graph </header>     
    <section class="card">
    <div class="card-header">
      <div class="form-row">
        <div class="form-group col col-md-4">
          <label for="inputEmail4">Select Device</label>
          <select class="form-control form-control-sm" id="device" name="device"> 
            <?php foreach($devices as $device){ ?>
                <option value="<?php echo $device->id ?>"><?php echo $device->device_name ?></option>
            <?php } ?>
          </select>
        </div>
        <div class="form-group col col-md-4">
          <label for="inputEmail4">Select Sensor</label>
          <select class="form-control form-control-sm" id="sensor" name="sensor">
            <?php foreach($sensors as $sensor){ ?>
                <option value="<?php echo $sensor->id ?>"><?php echo $sensor->name ?></option>
            <?php } ?>
          </select>
        </div>
        <div class="form-group col col-md-3">
        <label for="inputEmail4">Date</label>
          <input id="date" type="text" name="date" class="form-control form-control-sm datepicker_range" autocomplete="off">
        </div>        
        <div class="form-group col col-md-1" style="padding-top: 30px;">
          <button class="btn-primary btn-sm" id="btn_go">Go</button>
        </div>
      </div>
      <div id="container">
        
      </div>
      <svg id="loader" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="margin:auto;background:#fff;display:block;" width="197px" height="197px" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid">
        <circle cx="50" cy="50" r="0" fill="none" stroke="#e90c59" stroke-width="1">
          <animate attributeName="r" repeatCount="indefinite" dur="1.7543859649122806s" values="0;51" keyTimes="0;1" keySplines="0 0.2 0.8 1" calcMode="spline" begin="0s"></animate>
          <animate attributeName="opacity" repeatCount="indefinite" dur="1.7543859649122806s" values="1;0" keyTimes="0;1" keySplines="0.2 0 0.8 1" calcMode="spline" begin="0s"></animate>
        </circle><circle cx="50" cy="50" r="0" fill="none" stroke="#46dff0" stroke-width="1">
          <animate attributeName="r" repeatCount="indefinite" dur="1.7543859649122806s" values="0;51" keyTimes="0;1" keySplines="0 0.2 0.8 1" calcMode="spline" begin="-0.8771929824561403s"></animate>
          <animate attributeName="opacity" repeatCount="indefinite" dur="1.7543859649122806s" values="1;0" keyTimes="0;1" keySplines="0.2 0 0.8 1" calcMode="spline" begin="-0.8771929824561403s"></animate>
        </circle>
      </svg>
	</div>

@section('adminlte_js')
<script src="{{ URL::asset('plugins/Highcharts/code/highstock.js')}}"></script>
<script src="{{ URL::asset('plugins/Highcharts/code/modules/data.js')}}"></script>
<script src="{{ URL::asset('plugins/Highcharts/code/modules/exporting.js')}}"></script>
<script src="{{ URL::asset('plugins/Highcharts/code/modules/export-data.js')}}"></script>
<script src="{{ URL::asset('plugins/Highcharts/code/modules/accessibility.js')}}"></script>

<script>


$(function() {
  $("#graph_section").hide();
  $('#date').daterangepicker();
});

$('#date').on('apply.daterangepicker', function(ev, picker) {
  $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
});

$(document).ready(function(){
  $('#loader').hide();
});

$(document).ajaxStart(function () {
  $('#loader').show();
  $('#container').hide();
});

$(document).ajaxComplete(function () {
  $('#loader').hide();
  $('#container').show();
});

$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': "{{ csrf_token() }}",
  }
});

$(document).on('click','#btn_go',function(){
  var device_id = $('#device').val();
  var sensor_id = $('#sensor').val();
  var type = $('#type').val();
  var date = $('#date').val();
  $.ajax({
      
      url : "{{ route('get_graph_data') }}",
      type: "POST",
      data : {
        device_id : device_id,
        sensor_id : sensor_id,
        type : type,
        date : date,
      },
      success: function(data, textStatus, jqXHR)
      {
          data = JSON.parse(data);
          if(data.type == 'error'){
            //toastr.error(data.message);
          }
          if(data.type == 'success'){
            //set_data(data.xvalue,data.yvalue,data.min,data.max,data.stepsize);  
            var name = data.device_name;
            var data_val = data.xvalue;
            //var data_val = [[1663804800000,0.6],[1663804801000,0.6],[1663804802000,0.6],[1663804803000,0.6],[1663804804000,0.6],[1663804805000,0.6],[1663804806000,0.6],[1663804807000,0.6],[1663804808000,0.6],[1663804809000,0.6],[1663804810000,0.6],[1663804811000,0.6],[1663804812000,0.6],[1663804813000,0.6],[1663804814000,0.6],[1663804815000,0.6],[1663804816000,0.6],[1663804817000,0.6],[1663804818000,0.6],[1663804819000,0.6],[1663804820000,0.6],[1663804821000,0.6],[1663804822000,0.6],[1663804823000,0.6],[1663804824000,0.6]];
            generate_graph(data_val,name);
            $("#graph_section").show();
          }
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
                  
      }
  });
});


function generate_graph(data,name){
    
  // Create the chart
  Highcharts.stockChart('container', {
    chart: {
      zoomType: "x",
      resetZoomButton: {
        position: {
          // align: 'right', // by default
          // verticalAlign: 'top', // by default
          x: 50,
          y: 10
        },
      },
      events: {
        load: function() {
          const chart = this;
          const x = chart.plotLeft;
          const y = chart.plotTop;
          chart.renderer.button('RESET', x, y)
            .on('click', () => {
              chart.xAxis[0].setExtremes();
            })
            .add()
            .toFront();
        }
      }
    },
    title: {
      text: name
    },
    series: [{
      name: name,
      data: data,
      tooltip: {
          valueDecimals: 1
      }
    }]
  });
}
            
		</script>
	</body>
</html>
@stop
@stop
