@extends('adminlte::page',['sidebar' => true])
@section('title', 'Graph')

@section('content_header')
@stop

@section('content')
@php $data=[]; $i=1; @endphp
{{--@for($i=1;$i<=$device_count;$i++)  --}}
  @foreach($device as $d)
      @php $sensors = App\Models\Sensor::whereIn('id',json_decode($d->sensor))->get(); @endphp      
        @foreach($sensors as $sensor)
          @php $tmp=[];
          $tmp['i'] = $i;
          $tmp['name'] = $sensor->name;
          $tmp['min']=$sensor->min;
          $tmp['max']=$sensor->max;
          $data[]=$tmp; @endphp
        @endforeach 
        @php $i++; @endphp        
  @endforeach
{{--@endfor--}}


<div class="row state-overview">
  <div class="col-lg-12">
    <section class="p-2">
      <header class="m-2"> User DashBoard </header>        
          @foreach($data as $data)
            <div class="row">
                  <div class="col-sm-12 col-md-9 col-lg-9">
                    <div class="card mb-2 p-2 bg-white rounded">       
                      <div class="card-body p-0">                      
                        {{-- <canvas id="barChart_<?php echo $sensor->name.''.$i;?>" ></canvas> --}}
                        <canvas id="barChart_<?php echo $data['name'].''.$data['i'];?>"width="473" height="215" class="chartjs-render-monitor" style="display: block; width: 654px; height: 330px;"></canvas>                      
                      </div>
                    </div>
                  </div>
                
                <div class="col-sm-12 col-md-3 col-lg-3 text-center"> 
                  <div class="card p-2 mb-5 bg-white rounded w-100">
                    <div class="card-header p-0"><?php echo $data['name']; ?></div>
                      <div class="card-body">
                        <svg class="circle-chart" viewbox="0 0 33.83098862 33.83098862" xmlns="http://www.w3.org/2000/svg">'
                          <circle class="circle-chart__background" cx="16.9" cy="16.9" r="15.9" />'
                          <circle class="circle-chart__circle" id="circle_<?php echo $data['name'].$data['i']; ?>" stroke-dasharray="" cx="16.9" cy="16.9" r="15.9" />'
                            <g class="circle-chart__info">'
                              <text class="circle-chart__percent" x="16.9" y="14.5" id="circle_val_<?php echo $data['name'].$data['i']; ?>"  >0</text>';
                              <text class="circle-chart__subline" x="16.91549431" y="22" id="circle_time_<?php echo $data['name'].$data['i']; ?>">00:00:00</text>'
                            </g>
                        </svg>
                      </div>
                    <div class="card-footer p-1">
                      <div class="row">
                        <div class="col-6">
                          <lable>Max</label>
                          <p class="m-0"><?php echo $data['max']; ?></p>
                        </div>
                        <div class="col-6">
                          <lable>Min</label>
                          <p class="m-0"><?php echo $data['min']; ?></p>
                        </div>
                      </div>
                    </div>
                </div> 
                </div>    
                <h3 id="last_val_<?php echo $data['name'].''.$data['i']?>" style="display:none"></h3>                       
            </div>
            <hr>          
          @endforeach
    
@section('adminlte_js')



<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.7.1/chart.js"></script>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="https://github.com/chartjs/Chart.js/releases/download/v2.6.0/Chart.min.js"></script>

<script src="{{ URL::asset('plugins/jQuery-Progress-Circle/progresscircle.js')}}"></script>

<script>
@if(Session::has('message'))
var Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 2000,
        timerProgressBar: true,
        onOpen: function(toast) {
        toast.addEventListener('mouseenter', Swal.stopTimer)
        toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    });

    Toast.fire({
        icon: 'success',
        title: '{{ Session::get('message') }}'
    });
@endif

@php
foreach($device as $d){
  $sensors = App\Models\Sensor::whereIn('id',json_decode($d->sensor))->get(); 
  foreach($sensors as $sensor){ @endphp
    var myBarChart_<?php echo $sensor->name?> = [];    
    @php }
  }
@endphp
 
// var myBarChart1;
//var myBarChart=[];
// var ctx=[];
$.ajaxSetup({
   headers: {
     'X-CSRF-TOKEN': "{{ csrf_token() }}",
   }
});

  var no = 9; //Array 

  $.ajax({
    url : "{{ route('get_graph') }}",
    type: "POST",
    data : {
      no : no,
    },
    success: function(data, textStatus, jqXHR)
    {
      data = JSON.parse(data);
      if(data.type == 'success'){
        //$('#last_val').html(data.last_val);
        //generate_graph(data.xvalue,data.yvalue,data.max,data.min);
        //console.log(data.xvalue);
        //console.log(data.xvalue_1);

        for(var i=1;i<= <?php echo $device_count; ?>;i++){

            @php
            foreach($sensors as $sensor){ @endphp
                var canvas@php echo $sensor->name @endphp = document.getElementById("barChart_@php echo $sensor->name @endphp"+i);
                var ctx@php echo $sensor->name @endphp = canvas@php echo $sensor->name @endphp.getContext('2d');
                var chartType = 'line';
                //console.log(data.xvalue[i]);
                var max_len = data.yvalues_<?php echo $sensor->name?>[i].length;
                if(max_len > 0){
                  var max_len = data.yvalues_<?php echo $sensor->name?>[i].length - 1;
                }

                $('#last_val_<?php echo $sensor->name?>'+i).html(data.yvalues_<?php echo $sensor->name?>[i][max_len]);
                var final_per = get_percentage(<?php echo $sensor->max ?>,parseFloat(data.yvalues_<?php echo $sensor->name?>[i]));                
                $('#circle_<?php echo $sensor->name?>'+i).attr('stroke-dasharray',final_per+",100");
                $('#circle_val_<?php echo $sensor->name?>'+i).html(parseFloat(data.yvalues_<?php echo $sensor->name?>[i][max_len]));
                $('#circle_time_<?php echo $sensor->name?>'+i).html(data.xvalue[i][8]);


                xv= data.xvalue[i];
                yv<?php echo $sensor->name?>=data.yvalues_@php echo $sensor->name @endphp[i];

                var gradient = ctx@php echo $sensor->name @endphp.createLinearGradient(0, 0, 0, 300);
                gradient.addColorStop(0,"#E3E1F9");   
                gradient.addColorStop(1,"#FBFBFF");

                var d@php echo $sensor->name@endphp = {
                  labels:xv,
                  datasets: [{
                    label: "Device "+i,
                    fill: true,
                    lineTension: 0.5,
                    backgroundColor: gradient,
                    borderColor: "#696BFA", // The main line color
                    borderCapStyle: 'square',
                    pointBorderColor: "white",
                    pointBackgroundColor: "#696BFA",
                    pointBorderWidth: 1,
                    pointHoverRadius: 8,
                    pointHoverBackgroundColor: "#696BFA",
                    pointHoverBorderColor: "#696BFA",
                    pointHoverBorderWidth: 2,
                    pointRadius: 4,
                    pointHitRadius: 10,
                    data: yv@php echo $sensor->name @endphp,
                    spanGaps: true,
                  }],
                }

                //myBarChart = myBarChart[i];
                //ctx = ctx[i];
                myBarChart_@php echo $sensor->name @endphp[i] = new Chart(ctx@php echo $sensor->name @endphp, {
                  type: chartType,
                  data: d@php echo $sensor->name @endphp,
                  options: {
                    responsive: true,
                    maintainAspectRatio: false,
                      scales: {
                        
                          xAxes: [{
                            ticks: {
                              fontSize: 12,
                              display: true,           
                            },
                            gridLines: {
                                    display:false,
                              }     
                          }],
                          yAxes: [{
                            ticks: {          
                              fontSize: 12,
                              display: true,
                              @if($sensor->name == 'V')
                                max:{{$sensor->max}} ,
                                min:{{$sensor->min}},
                                stepSize: 1,
                              @elseif($sensor->name == 'I')
                                max:{{$sensor->max}},
                                min:{{$sensor->min}},
                                stepSize: 10,
                              @endif
                            }, 
                            gridLines: {
                                    borderDash: [4, 4],
                              }     
                          }]
                        },
                      title: {
                        fontSize: 18,
                        display: true,
                        position: 'bottom'
                      }
                    },
                });
          @php } @endphp
        }
      }
    },
  });

// var options = {
//   scales: {
    
//        xAxes: [{
//          ticks: {
//            fontSize: 12,
//            display: true,           
//          },
//          gridLines: {
//                 display:false,
//           }     
//        }],
//        yAxes: [{
//          ticks: {          
//            fontSize: 12,
//            display: true,
//            max:5,
//            min:1,
//            stepSize: 1,
//          }, 
//          gridLines: {
//                 borderDash: [4, 4],
//           }     
//        }]
//      },
//   title: {
//     fontSize: 18,
//     display: true,
//     position: 'bottom'
//   }
// };


// function init(d) {
//   // Chart declaration:
//   myBarChart = new Chart(ctx, {
//     type: chartType,
//     data: d,
//     options: options
//   });

//   myBarChart1 = new Chart(ctx1, {
//     type: chartType,
//     data: d,
//     options: options
//   });
// }

$(document).ajaxComplete(function () {
  get_data();
  setInterval(function() {
    get_data();
    //$('#mydiv').data('myval',20); attr
  }, 5000000);
});

function get_data(){
  var no = 1; //Array 
  $.ajax({
      url : "{{ route('get_graph') }}",
      type: "POST",
      data : {
        no : no,
      },
      success: function(d, textStatus, jqXHR)
      {
        chart_data = JSON.parse(d);
        //var last = get_last_values();
       
       //console.log(last);
       //console.log(typeof last[1]);
        if(chart_data.type == 'success'){
          for(var i=1;i<= <?php echo $device_count; ?>;i++){

            //console.log(chart_data.yvalue[i]);
            //myBarChart = myBarChart[i];
            //console.log(myBarChart.data.labels);
            // removeData();
            // pushData(chart_data.xvalue[i],chart_data.yvalue[i]);
            //console.log(chart_data.xvalue[i]);
            //myBarChart = myBarChart+i
            // console.log(myBarChart.data.labels);
            //myBarChart = myBarChart+i;
            //var yVal = 15
            //yVal = yVal + Math.round(5 + Math.random() * (-5 - 5));
            
            <?php 
              //$sensors = Sensor::whereIn('id',json_decode($d->sensor))->get();
              foreach($sensors as $sensor){ ?>
                var a = chart_data.yvalues_<?php echo $sensor->name?>[i][0];
                //console.log(a);                
                if($('#last_val_<?php echo $sensor->name?>'+i).html() != a){
                  myBarChart_<?php echo $sensor->name?>[i].data.labels.splice(0, 1);
                  myBarChart_<?php echo $sensor->name?>[i].data.datasets[0].data.splice(0, 1);
                  myBarChart_<?php echo $sensor->name?>[i].data.labels.push(chart_data.xvalue[i]);
                  myBarChart_<?php echo $sensor->name?>[i].data.datasets[0].data.push(parseFloat(chart_data.yvalues_<?php echo $sensor->name?>[i]));
                  $('#last_val_<?php echo $sensor->name?>'+i).html(chart_data.yvalues_<?php echo $sensor->name?>[i]);
                  var final_per = get_percentage(<?php echo $sensor->max?>,parseFloat(chart_data.yvalues_<?php echo $sensor->name?>[i]));                
                  $('#circle_<?php echo $sensor->name?>'+i).attr('stroke-dasharray',final_per+",100");
                  $('#circle_val_<?php echo $sensor->name?>'+i).html(parseFloat(chart_data.yvalues_<?php echo $sensor->name?>[i]));
                  $('#circle_time_<?php echo $sensor->name?>'+i).html(chart_data.xvalue[i]);
                  myBarChart_<?php echo $sensor->name?>[i].update();
                }
              <?php } ?>
          }
        }
      },
  });
}

function get_last_values(){
  var last_data = [];
  last_data.push(null);
  for(var i=1;i<= <?php echo $device_count; ?>;i++){        
  <?php 
    //$sensors = Sensor::whereIn('id',json_decode($d->sensor))->get();
    foreach($sensors as $sensor){ ?>
      last_data.push($('#last_val_<?php echo $sensor->name?>'+i).html());
    <?php } ?>
    }
  return last_data;
}


</script>
@stop
@stop
