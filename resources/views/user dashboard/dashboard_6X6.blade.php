@extends('adminlte::page',['sidebar' => true])
@section('title', 'Graph')
@section('adminlte_css')
<style>
  

  .card_hover{ 
    box-shadow: 1px 3px 3px grey;
  }

  .font-larger{
    font-size: larger;

  }

  </style>
  @endsection
@section('content_header')
@stop

@section('content')
@php $data=[]; $i=1; @endphp
{{--@for($i=1;$i<=$device_count;$i++)  --}}
  @foreach($device as $d)
    @php
      $jmp=[];
    @endphp
      @php $sensors = App\Models\Sensor::whereIn('id',json_decode($d->sensor))->get(); @endphp      
        @foreach($sensors as $sensor)
          @php 
          $tmp = [];
          $tmp['i'] = $i;
          $tmp['device_name'] = $d->device_name;
          $tmp['name'] = $sensor->name;
          $tmp['min']=$sensor->min;
          $tmp['max']=$sensor->max;
          $jmp[$d->device_name][]=$tmp;
          @endphp
        @endforeach 
        @php $data[]=$jmp; @endphp
        @php $i++; @endphp        
  @endforeach
{{--@endfor--}}

<div class="row state-overview">
  <div class="col-lg-12">
    <section class="p-2">
      <header class="m-2"> User DashBoard</header>   
        <div class="row gx-5">     
          @foreach($data as $key=>$j)
            <div class="col-sm-12 col-md-12 col-lg-12">
              <div class="card p-1 bg-white rounded mb-1">
                <h5 class="m-0 text-center">{{key($j)}}</h5>      
              </div>
            </div>
            @foreach ($j as $d)
              @foreach($d as $m)
              <div class="col-sm-12 col-md-6 col-lg-6">
                <div class="card p-1 bg-white rounded mb-1 card_hover">
                  <div class="row">
                    <div class="col-12">
                      <div class="card-body p-0">                      
                        <canvas id="barChart_<?php echo $m['name'].''.$m['i'];?>"width="473" height="215" class="chartjs-render-monitor" style="display: block; width: 654px; height: 330px;"></canvas>                      
                      </div>
                    </div>                  
                  </div> 
                </div>         
                <div class="card p-1 bg-white rounded card_hover">
                  {{-- <div class="row">                                    
                    <div class="col-3 text-center bg-dark">
                      Time
                    </div>
                    <div class="col-3 text-center bg-primary">
                      @if($m['name'] == 'V')
                        Voltage
                      @elseif ($m['name'] == 'I')
                        Current  
                      @endif
                    </div>
                    <div class="col-3 text-center bg-secondary">
                      Min
                    </div>
                    <div class="col-3 text-center bg-secondary">
                      Max
                    </div>
                    <div class="col-3 text-center bg-dark">
                      <span id="circle_time_<?php echo $m['name'].$m['i']; ?>">00:00:00</span>
                    </div>
                    <div class="col-3 text-center bg-primary">
                      <span id="circle_val_<?php echo $m['name'].$m['i']; ?>">0</span>  {{' '.$m['name']}}
                    </div>
                    <div class="col-3 text-center bg-secondary">
                      {{$m['min']}}
                    </div>as $d)
              @foreach($d as $m)
              <div class="col-sm-12 col-md-6 col-lg-6">
                <div class="card p-1 bg-white rounded mb-1">
                  <div class="row"
                    <div class="col-3 text-center bg-secondary">
                      {{$m['max']}}
                    </div>
                  </div> --}}

                  <div class="row">                                    
                    <div class="col-3 text-center m-auto">
                      @if($m['name'] == 'V')
                        <img src="{{  url("image/voltage.png") }}">
                      @elseif ($m['name'] == 'I')
                        <img src="{{  url("image/current.png") }}">
                      @endif
                      
                    </div>
                    <div class="col-3 text-left m-auto p-0">
                      <h3><span id="circle_val_<?php echo $m['name'].$m['i']; ?>">0</span>  {{' '.$m['name']}} </h3>
                    </div>
                    <div class="col-6 text-center m-auto">
                      <div class="row">
                        <div class="col-12 mb-3">
                          <i class="fa fa-lg fa-clock">&nbsp;</i><span class="font-larger" id="circle_time_<?php echo $m['name'].$m['i']; ?>">00:00:00</span>
                        </div>
                        <div class="col-12">
                          <div class="row">
                            <div class="col-6"><i class="fa fa-md	fa-arrow-circle-up">&nbsp;</i><span class="font-larger">Max : {{$m['max']}}</span></div>
                            <div class="col-6"><i class="fa fa-md	fa-arrow-circle-down">&nbsp;</i><span class="font-larger">Min : {{$m['min']}}</span></div>
                          </div>
                        </div>
                      </div>
                    </div>                    
                  </div>
                </div>
              </div>                                
              <h3 id="last_val_<?php echo $m['name'].''.$m['i']?>" style="display:none"></h3>
              @endforeach
            @endforeach
          @endforeach
        </div>

    
@section('adminlte_js')



<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.7.1/chart.js"></script>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="https://github.com/chartjs/Chart.js/releases/download/v2.6.0/Chart.min.js"></script>

<script src="{{ URL::asset('plugins/jQuery-Progress-Circle/progresscircle.js')}}"></script>

<script>
@if(Session::has('message'))
var Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 2000,
        timerProgressBar: true,
        onOpen: function(toast) {
        toast.addEventListener('mouseenter', Swal.stopTimer)
        toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    });

    Toast.fire({
        icon: 'success',
        title: '{{ Session::get('message') }}'
    });
@endif

@php
foreach($device as $d){
  $sensors = App\Models\Sensor::whereIn('id',json_decode($d->sensor))->get(); 
  foreach($sensors as $sensor){ @endphp
    var myBarChart_<?php echo $sensor->name?> = [];    
    @php }
  }
@endphp
 
// var myBarChart1;
//var myBarChart=[];
// var ctx=[];
$.ajaxSetup({
   headers: {
     'X-CSRF-TOKEN': "{{ csrf_token() }}",
   }
});

  var no = 9; //Array 

  $.ajax({
    url : "{{ route('get_graph') }}",
    type: "POST",
    data : {
      no : no,
    },
    success: function(data, textStatus, jqXHR)
    {
      data = JSON.parse(data);
      if(data.type == 'success'){
        //$('#last_val').html(data.last_val);
        //generate_graph(data.xvalue,data.yvalue,data.max,data.min);
        //console.log(data.xvalue);
        //console.log(data.xvalue_1);

        for(var i=1;i<= <?php echo $device_count; ?>;i++){

            @php
            foreach($sensors as $sensor){ @endphp
                var canvas@php echo $sensor->name @endphp = document.getElementById("barChart_@php echo $sensor->name @endphp"+i);
                var ctx@php echo $sensor->name @endphp = canvas@php echo $sensor->name @endphp.getContext('2d');
                var chartType = 'line';
                //console.log(data.xvalue[i]);
                var max_len = data.xvalue[i].length;
                if(max_len > 0){
                  var max_len = data.xvalue[i].length - 1;
                }

                $('#last_val_<?php echo $sensor->name?>'+i).html(data.xvalue[i][max_len]);
                $('#circle_val_<?php echo $sensor->name?>'+i).html(parseFloat(data.yvalues_<?php echo $sensor->name?>[i][max_len]));
                $('#circle_time_<?php echo $sensor->name?>'+i).html(data.xvalue[i][8]);


                xv= data.xvalue[i];
                yv<?php echo $sensor->name?>=data.yvalues_@php echo $sensor->name @endphp[i];

                var gradient = ctx@php echo $sensor->name @endphp.createLinearGradient(0, 0, 0, 500);
                gradient.addColorStop(0,"#E3E1F9");   
                gradient.addColorStop(1,"#FBFBFF");

                var d@php echo $sensor->name@endphp = {
                  labels:xv,
                  datasets: [{
                    label: "@php echo $sensor->name @endphp",
                    fill: true,
                    lineTension: 0.5,
                    backgroundColor: gradient,
                    borderColor: "#696BFA", // The main line color
                    borderCapStyle: 'square',
                    pointBorderColor: "white",
                    pointBackgroundColor: "#696BFA",
                    pointBorderWidth: 1,
                    pointHoverRadius: 8,
                    pointHoverBackgroundColor: "#696BFA",
                    pointHoverBorderColor: "#696BFA",
                    pointHoverBorderWidth: 2,
                    pointRadius: 4,
                    pointHitRadius: 10,
                    data: yv@php echo $sensor->name @endphp,
                    spanGaps: true,
                  }],
                }

                //myBarChart = myBarChart[i];
                //ctx = ctx[i];
                myBarChart_@php echo $sensor->name @endphp[i] = new Chart(ctx@php echo $sensor->name @endphp, {
                  type: chartType,
                  data: d@php echo $sensor->name @endphp,
                  options: {
                    responsive: true,
                    maintainAspectRatio: false,
                      scales: {
                        
                          xAxes: [{
                            ticks: {
                              fontSize: 12,
                              display: true,           
                            },
                            gridLines: {
                                    display:false,
                              }     
                          }],
                          yAxes: [{
                            ticks: {          
                              fontSize: 12,
                              display: true,
                              @if($sensor->name == 'V')
                                max:{{$sensor->max}} ,
                                min:{{$sensor->min}},
                                stepSize: 1,
                              @elseif($sensor->name == 'I')
                                max:{{$sensor->max}},
                                min:{{$sensor->min}},
                                stepSize: 10,
                              @endif
                            }, 
                            gridLines: {
                                    borderDash: [4, 4],
                              }     
                          }]
                        },
                      title: {
                        fontSize: 18,
                        display: true,
                        position: 'bottom'
                      }
                    },
                });
          @php } @endphp
        }
      }
    },
  });

// var options = {
//   scales: {
    
//        xAxes: [{
//          ticks: {
//            fontSize: 12,
//            display: true,           
//          },
//          gridLines: {
//                 display:false,
//           }     
//        }],
//        yAxes: [{
//          ticks: {          
//            fontSize: 12,
//            display: true,
//            max:5,
//            min:1,
//            stepSize: 1,
//          }, 
//          gridLines: {
//                 borderDash: [4, 4],
//           }     
//        }]
//      },
//   title: {
//     fontSize: 18,
//     display: true,
//     position: 'bottom'
//   }
// };


// function init(d) {
//   // Chart declaration:
//   myBarChart = new Chart(ctx, {
//     type: chartType,
//     data: d,
//     options: options
//   });

//   myBarChart1 = new Chart(ctx1, {
//     type: chartType,
//     data: d,
//     options: options
//   });
// }

$(document).ajaxComplete(function () {
  get_data();
  setInterval(function() {
    get_data();
    //$('#mydiv').data('myval',20); attr
  }, 5000000);
});

function get_data(){
  var no = 1; //Array 
  $.ajax({
      url : "{{ route('get_graph') }}",
      type: "POST",
      data : {
        no : no,
      },
      success: function(d, textStatus, jqXHR)
      {
        chart_data = JSON.parse(d);
        //var last = get_last_values();
       
       //console.log(last);
       //console.log(typeof last[1]);
        if(chart_data.type == 'success'){
          for(var i=1;i<= <?php echo $device_count; ?>;i++){

            //console.log(chart_data.yvalue[i]);
            //myBarChart = myBarChart[i];
            //console.log(myBarChart.data.labels);
            // removeData();
            // pushData(chart_data.xvalue[i],chart_data.yvalue[i]);
            //console.log(chart_data.xvalue[i]);
            //myBarChart = myBarChart+i
            // console.log(myBarChart.data.labels);
            //myBarChart = myBarChart+i;
            //var yVal = 15
            //yVal = yVal + Math.round(5 + Math.random() * (-5 - 5));
            
            <?php 
              //$sensors = Sensor::whereIn('id',json_decode($d->sensor))->get();
              foreach($sensors as $sensor){ ?>
                var a = chart_data.xvalue[i][0];
                
                if($('#last_val_<?php echo $sensor->name?>'+i).html() != a){
                  myBarChart_<?php echo $sensor->name?>[i].data.labels.splice(0, 1);
                  myBarChart_<?php echo $sensor->name?>[i].data.datasets[0].data.splice(0, 1);
                  myBarChart_<?php echo $sensor->name?>[i].data.labels.push(chart_data.xvalue[i]);
                  myBarChart_<?php echo $sensor->name?>[i].data.datasets[0].data.push(parseFloat(chart_data.yvalues_<?php echo $sensor->name?>[i]));
                  $('#last_val_<?php echo $sensor->name?>'+i).html(chart_data.xvalue[i]);
                  var final_per = get_percentage(<?php echo $sensor->max?>,parseFloat(chart_data.yvalues_<?php echo $sensor->name?>[i]));                
                  $('#circle_<?php echo $sensor->name?>'+i).attr('stroke-dasharray',final_per+",100");
                  $('#circle_val_<?php echo $sensor->name?>'+i).html(parseFloat(chart_data.yvalues_<?php echo $sensor->name?>[i]));
                  $('#circle_time_<?php echo $sensor->name?>'+i).html(chart_data.xvalue[i]);
                  myBarChart_<?php echo $sensor->name?>[i].update();
                }
        <?php } ?>
          }
        }
      },
  });
}

function get_last_values(){
  var last_data = [];
  last_data.push(null);
  for(var i=1;i<= <?php echo $device_count; ?>;i++){        
  <?php 
    //$sensors = Sensor::whereIn('id',json_decode($d->sensor))->get();
    foreach($sensors as $sensor){ ?>
      last_data.push($('#last_val_<?php echo $sensor->name?>'+i).html());
    <?php } ?>
    }
  return last_data;
}


</script>
@stop
@stop
