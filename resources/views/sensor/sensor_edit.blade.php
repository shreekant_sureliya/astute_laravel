@extends('adminlte::page',['sidebar' => true])
@section('title', 'Sensor-create')

@section('content_header')
@stop

@section('content')

@if(session()->has('success'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
  {{ session()->get('success') }}
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif

<div class="row p-2">
    <h5>Edit Sensor</h5>
</div>
<div class="row p-3 card">
    <div class="card-body">
    <form class="form-horizontal"  action="{{route('sensor.update',['id'=>$sensor->id])}}" method="post">
        @csrf
            <div class="form-group row">
              <label for="device_id" class="col-sm-2 control-label">Name</label>
                  <div class="col-sm-10">
                      <input name="name" type="text" value="{{$sensor->name}}" class="form-control   @error('name') is-invalid @enderror" placeholder="Enter Name">
                  @error('name')
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                      </span>
                  @enderror
                  </div>
            </div>
            <div class="form-group row">
              <label for="max_value" class="col-sm-2 control-label">Max Value</label>
                <div class="col-sm-6">
                    <input name="max" type="number" value="{{$sensor->max}}" class="form-control   @error('max') is-invalid @enderror" placeholder="Enter Max Value">
                @error('max')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                </div>
            </div>       
            <div class="form-group row">
              <label for="device_id" class="col-sm-2 control-label">Min Value</label>
                <div class="col-sm-6">
                    <input name="min" type="number" value="{{$sensor->min}}" class="form-control   @error('min') is-invalid @enderror" placeholder="Enter Min Value">
                @error('min')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                </div>
            </div>                     
            <div class="box-footer">
                <button type="submit" class="btn btn-sm btn-primary">Update</button>
                <a href="{{route('sensor-manage')}}" class="btn btn-sm btn-default">Cancel</a>
            </div>
        </form>
    </div>
</div>

@section('adminlte_js')
<script>
@if(Session::has('message'))
var Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 2000,
        timerProgressBar: true,
        onOpen: function(toast) {
        toast.addEventListener('mouseenter', Swal.stopTimer)
        toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    });

    Toast.fire({
        icon: 'success',
        title: '{{ Session::get('message') }}'
    });
@endif
</script>
@stop
@stop
